MODULE LaserFieldMDL
use UnitAndConstantMDL
use InputOutputMDL
implicit none
type LaserParameter
  character(LEN=256) :: envelope
  real(kind=dp) :: amplitude
  real(kind=dp) :: AngularFrequency
  real(kind=dp) :: PhaseShift
  real(kind=dp) :: TimeShift
  real(kind=dp) :: EnvelopeHeight
  real(kind=dp) :: EnvelopeTime1
  real(kind=dp) :: EnvelopeTime2
  real(kind=dp) :: EnvelopeProperty1
  real(kind=dp) :: EnvelopeProperty2
endtype LaserParameter


interface BasicFunctionEvaluation
  module procedure BasicFunctionEvaluationWithSingleInput
  module procedure BasicFunctionEvaluationWithVectorInput
end interface BasicFunctionEvaluation

interface LaserField
  module procedure LaserFieldWithSingleColorWithVectorInput
  module procedure LaserFieldWithSingleColorWithSingleInput
  module procedure LaserFieldWithManyColorsWithVectorInput
  module procedure LaserFieldWithManyColorsWithSingleInput
end interface LaserField


private

public LaserFieldParameterConversion

public BasicFunctionEvaluation, &
       LaserField, &
       LaserFieldMDL_Tester

! old subroutines and functions
public LaserFieldGeneratorFunction, &
       WindowGeneratorFunction

public LaserFieldGenerator, &
       LaserFieldGeneratorNew, &
       WindowGenerator


contains


  !----------------------------------------------------------------
  !>
  !! Format for FromThis and ToThis:
  !!   From/To [Physical quantity]: [Unit of the Physical quantity]
  !! Acceptable units for electric field amplitude family: 
  !!   Electric Field Amplitude: au, V/A, V/nm
  !!   Intensity               : au, TW/cm^2
  !! Acceptable units for angularFrequency famility:
  !!   Angular Frequency: au
  !!   Frequency        : au, THz
  !!   Period           : au, fs, as
  !!   Photon Energy    : au, eV
  !!   Wavelength       : au, nm
  !!
  function LaserFieldParameterConversion(FromThis, Input, ToThis)
    implicit none
    real(kind=dp) :: LaserFieldParameterConversion
    character(len=*), intent(in) :: FromThis
    character(len=*), intent(in) :: ToThis
    real(kind=dp), intent(in) :: Input

    ! Internal
    real(kind=dp) :: ElectricFieldAmplitude
    real(kind=dp) :: LaserIntensity
    real(kind=dp) :: AngularFrequency
    real(kind=dp) :: frequency
    real(kind=dp) :: period
    real(kind=dp) :: PhotonEnergy
    real(kind=dp) :: wavelength

    ! Input
    select case (trim(adjustl(FromThis(1:scan(FromThis, ':') - 1)))) 
      case ('From Electric Field Amplitude', 'From Intensity')
        select case (trim(adjustl(FromThis)))
          case ('From Electric Field Amplitude: au')
            ElectricFieldAmplitude = Input
            LaserIntensity = ElectricFieldAmplitude**2
          case ('From Electric Field Amplitude: V/A')
            ElectricFieldAmplitude = Input*u_VoltOverAngstrom
            LaserIntensity = ElectricFieldAmplitude**2
          case ('From Electric Field Amplitude: V/nm')
            ElectricFieldAmplitude = Input*u_VoltOverAngstrom/10.d0
            LaserIntensity = ElectricFieldAmplitude**2
          case ('From Intensity: au')
            LaserIntensity = input
            ElectricFieldAmplitude = dsqrt(LaserIntensity)
          case ('From Intensity: TW/cm^2')
            LaserIntensity = input*u_TW_Over_CM2
            ElectricFieldAmplitude = dsqrt(LaserIntensity)
          case default
            call MessageText('Error', 'Invalid name for input quantity.', 'LaserFieldMDL', 'LaserFieldParameterConversion')
        end select
      case ('From Angular Frequency', 'From Frequency', 'From Period', 'From Photon Energy', 'From Wavelength')
        select case (trim(adjustl(FromThis)))
          case ('From Angular Frequency: au')
            AngularFrequency = input
            frequency = AngularFrequency/2.d0/pi
            period = 2.d0*pi/AngularFrequency
            PhotonEnergy = AngularFrequency
            wavelength = 2.d0*pi/AngularFrequency*u_LightSpeed
          case ('From Frequency: au')
            frequency = input
            AngularFrequency = 2.d0*pi*frequency
            period = 1.d0/frequency
            PhotonEnergy = 2.d0*pi*frequency
            wavelength = 1.d0/frequency*u_LightSpeed
          case ('From Frequency: THz')
            frequency = input*1.d12/usi_S
            AngularFrequency = 2.d0*pi*frequency
            period = 1.d0/frequency
            PhotonEnergy = 2.d0*pi*frequency
            wavelength = 1.d0/frequency*u_LightSpeed
          case ('From Period: au')
            period = input
            AngularFrequency = 2.d0*pi/period
            frequency = 1.d0/period
            PhotonEnergy = 2.d0*pi/period
            wavelength = period*u_LightSpeed
          case ('From Period: fs')
            period = input*u_FemtoSecond
            AngularFrequency = 2.d0*pi/period
            frequency = 1.d0/period
            PhotonEnergy = 2.d0*pi/period
            wavelength = period*u_LightSpeed
          case ('From Period: as')
            period = input*u_AttoSecond
            AngularFrequency = 2.d0*pi/period
            frequency = 1.d0/period
            PhotonEnergy = 2.d0*pi/period
            wavelength = period*u_LightSpeed
          case ('From Photon Energy: au')
            PhotonEnergy = input
            AngularFrequency = PhotonEnergy
            frequency = PhotonEnergy/2.d0/pi
            period = 2.d0*pi/PhotonEnergy
            wavelength = 2.d0*pi/PhotonEnergy*u_LightSpeed
          case ('From Photon Energy: eV')
            PhotonEnergy = input*u_eV
            AngularFrequency = PhotonEnergy
            frequency = PhotonEnergy/2.d0/pi
            period = 2.d0*pi/PhotonEnergy
            wavelength = 2.d0*pi/PhotonEnergy*u_LightSpeed
          case ('From Wavelength: au')
            wavelength = input
            AngularFrequency = 2.d0*pi/wavelength*u_LightSpeed
            frequency = u_LightSpeed/wavelength
            period = wavelength/u_LightSpeed
            PhotonEnergy = 2.d0*pi/wavelength*u_LightSpeed
          case ('From Wavelength: nm')
            wavelength = input*u_NanoMeter
            AngularFrequency = 2.d0*pi/wavelength*u_LightSpeed
            frequency = u_LightSpeed/wavelength
            period = wavelength/u_LightSpeed
            PhotonEnergy = 2.d0*pi/wavelength*u_LightSpeed
          case default
            call MessageText('Error', 'Invalid name for input quantity.', 'LaserFieldMDL', 'LaserFieldParameterConversion')
        end select
      case default
        call MessageText('Error', 'Invalid family name for input quantity.', 'LaserFieldMDL', 'LaserFieldParameterConversion')
    end select

    ! Output
    select case (trim(adjustl(ToThis(1:scan(ToThis, ':') - 1)))) 
      case ('To Electric Field Amplitude', 'To Intensity')
        select case (trim(adjustl(ToThis)))
          case ('To Electric Field Amplitude: au')
            LaserFieldParameterConversion = ElectricFieldAmplitude
          case ('To Electric Field Amplitude: V/A')
            LaserFieldParameterConversion = ElectricFieldAmplitude/u_VoltOverAngstrom
          case ('To Electric Field Amplitude: V/nm')
            LaserFieldParameterConversion = ElectricFieldAmplitude/u_VoltOverAngstrom*10.d0
          case ('To Intensity: au')
            LaserFieldParameterConversion = LaserIntensity
          case ('To Intensity: TW/cm^2')
            LaserFieldParameterConversion = LaserIntensity/u_TW_Over_CM2
          case default
            call MessageText('Error', 'Invalid name for output quantity.', 'LaserFieldMDL', 'LaserFieldParameterConversion')
        end select
      case ('To Angular Frequency', 'To Frequency', 'To Period', 'To Photon Energy', 'To Wavelength')
        select case (trim(adjustl(ToThis)))
          case ('To Angular Frequency: au')
            LaserFieldParameterConversion = AngularFrequency
          case ('To Frequency: au')
            LaserFieldParameterConversion = frequency
          case ('To Frequency: THz')
            LaserFieldParameterConversion = frequency*usi_S*1.d-12
          case ('To Period: au')
            LaserFieldParameterConversion = period
          case ('To Period: fs')
            LaserFieldParameterConversion = period/u_FemtoSecond
          case ('To Period: as')
            LaserFieldParameterConversion = period/u_AttoSecond
          case ('To Photon Energy: au')
            LaserFieldParameterConversion = PhotonEnergy
          case ('To Photon Energy: eV')
            LaserFieldParameterConversion = PhotonEnergy/u_eV
          case ('To Wavelength: au')
            LaserFieldParameterConversion = wavelength
          case ('To Wavelength: nm')
            LaserFieldParameterConversion = wavelength/u_NanoMeter
          case default
            call MessageText('Error', 'Invalid name for output quantity.', 'LaserFieldMDL', 'LaserFieldParameterConversion')
        end select
      case default
        call MessageText('Error', 'Invalid family name for output quantity.', 'LaserFieldMDL', 'LaserFieldParameterConversion')
    end select

  end function LaserFieldParameterConversion


  !----------------------------------------------------------------
  !>
  subroutine LaserFieldWithSingleColorWithSingleInput(Time, LaserSetup, LaserField)
    implicit none
    real(kind=dp), intent(in) :: Time
    type(LaserParameter), intent(in) :: LaserSetup
    real(kind=dp), intent(out) :: LaserField

    ! Internal
    real(kind=dp) :: time_tmp(1)
    type(LaserParameter) :: LaserSetup_tmp(1)
    real(kind=dp) :: LaserField_tmp(1)

    time_tmp(1) = time
    LaserSetup_tmp(1) = LaserSetup
    call LaserFieldWithManyColorsWithVectorInput(1, 1, Time_tmp, LaserSetup_tmp, LaserField_tmp)
    LaserField = LaserField_tmp(1)

  end subroutine LaserFieldWithSingleColorWithSingleInput


  !----------------------------------------------------------------
  !>
  subroutine LaserFieldWithManyColorsWithSingleInput(NumberOfColors, Time, LaserSetup, LaserField)
    implicit none
    integer, intent(in) :: NumberOfColors
    real(kind=dp), intent(in) :: Time
    type(LaserParameter), intent(in) :: LaserSetup(NumberOfColors)
    real(kind=dp), intent(out) :: LaserField

    ! Internal
    real(kind=dp) :: time_tmp(1)
    real(kind=dp) :: LaserField_tmp(1)

    time_tmp(1) = time
    call LaserFieldWithManyColorsWithVectorInput(NumberOfColors, 1, Time_tmp, LaserSetup, LaserField_tmp)
    LaserField = LaserField_tmp(1)

  end subroutine LaserFieldWithManyColorsWithSingleInput


  !----------------------------------------------------------------
  !>
  subroutine LaserFieldWithSingleColorWithVectorInput(NumberOfTimeGrids, Time, LaserSetup, LaserField)
    implicit none
    integer, intent(in) :: NumberOfTimeGrids
    real(kind=dp), intent(in) :: Time(NumberOfTimeGrids)
    type(LaserParameter), intent(in) :: LaserSetup
    real(kind=dp), intent(out) :: LaserField(NumberOfTimeGrids)

    ! Internal
    type(LaserParameter) :: LaserSetup_tmp(1)
    
    LaserSetup_tmp(1) = LaserSetup
    call LaserFieldWithManyColorsWithVectorInput(1, NumberOfTimeGrids, Time, LaserSetup_tmp, LaserField)

  end subroutine LaserFieldWithSingleColorWithVectorInput


  !----------------------------------------------------------------
  !>
  !! This subroutine returns the laser field specified by the laser parameters.
  !! For each envelope y(t), p1, p2, ..., p5 correspond to EH, ET1, EP1, ET2, EP2(EH = EnvelopeHeight; ET = EnvelopeTime; EP = EnvelopeProperty).
  !! Here is a list for several envelopes for ease of matching parameters.
  !!   constant:
  !!     y(t) = EH
  !!   sine-quadratic
  !!     y(t) = EH*sin^2(2.d0*pi*(x - ET2)/T) ; T = EP1 - ET1
  !!   sine-quartic
  !!     y(t) = EH*sin^4(2.d0*pi*(x - ET2)/T) ; T = EP1 - ET1
  !!   gaussian
  !!     y(t) = EH*exp^(-0.5*((t - ET1)/EP1)^2)
  !!   lorentzian
  !!     y(t) = 1/(EP1*pi*(1 + ((x - ET1)/EP1)^2))
  !!   ramp-sine-up-constant-down
  !!     y(t) = 0 ; if t < ET1
  !!     y(t) = EH*sin(2*pi/T*(x - ET1)) , T = 4*(EP1 - ET1); if ET1 <= x < EP1
  !!     y(t) = EH ; if EP1 <= x < ET2
  !!     y(t) = EH*cos(2*pi/T*(x - ET2)) , T = 4*(EP2 - ET2); if ET2 <= x < EP2
  !!     y(t) = 0 ; if x >= EP2
  !!   ramp-sine-up
  !!     y(t) = 0 ; if x < ET1
  !!     y(t) = EH*sin(2*pi/T*(x - ET1)) , T = 4*(EP1 - ET1); if ET1 <= x < EP1
  !!     y(t) = EH ; if EP1 <= x
  !!   ramp-sine-down
  !!     y(t) = EH ; if x < ET1
  !!     y(t) = EH*cos(2*pi/T*(x - ET1)) , T = 4*(EP1 - ET1); if ET1 <= x < EP1
  !!     y(t) = 0 ; if EP1 <= x
  !!   ramp-gaussian-up-constant-down
  !!     y(t) = EH*exp^(-0.5*((x - ET1)/EP1)^2) ; if x < ET1
  !!     y(t) = EH ; if ET1 <= x < ET2
  !!     y(t) = EH*exp^(-0.5*((x - ET2)/EP2)^2) ; if ET2 <= x
  !!   ramp-gaussian-up
  !!     y(t) = EH*exp^(-0.5*((x - ET1)/EP1)^2) ; if x < ET1
  !!     y(t) = EH ; if ET1 <= x
  !!   ramp-gaussian-down
  !!     y(t) = EH ; if ET1 x < ET1
  !!     y(t) = EH*exp^(-0.5*((x - ET1)/EP1)^2) ; if ET1 <= x
  !!   
  subroutine LaserFieldWithManyColorsWithVectorInput(NumberOfColors, NumberOfTimeGrids, Time, LaserSetup, LaserField)
    implicit none
    integer, intent(in) :: NumberOfColors
    integer, intent(in) :: NumberOfTimeGrids
    real(kind=dp), intent(in) :: Time(NumberOfTimeGrids)
    type(LaserParameter), intent(in) :: LaserSetup(NumberOfColors)
    real(kind=dp), intent(out) :: LaserField(NumberOfTimeGrids)

    ! Internal
    integer :: i
    real(kind=dp) :: EnvelopePart(NumberOfTimeGrids)
    real(kind=dp) :: OscillatorPart(NumberOfTimeGrids)

    LaserField = 0.d0
    do i = 1, NumberOfColors
      call BasicFunctionEvaluation(trim(adjustl(LaserSetup(i)%envelope)), NumberOfTimeGrids, Time, EnvelopePart, LaserSetup(i)%EnvelopeHeight,&
                                                                                                                 LaserSetup(i)%EnvelopeTime1,&
                                                                                                                 LaserSetup(i)%EnvelopeProperty1,&
                                                                                                                 LaserSetup(i)%EnvelopeTime2,&
                                                                                                                 LaserSetup(i)%EnvelopeProperty2)
      call BasicFunctionEvaluation('cosine', NumberOfTimeGrids, Time, OscillatorPart, LaserSetup(i)%amplitude,&
                                                                                      LaserSetup(i)%AngularFrequency,&
                                                                                      LaserSetup(i)%PhaseShift,&
                                                                                      LaserSetup(i)%TimeShift)
      LaserField = LaserField + EnvelopePart*OscillatorPart
    end do

  end subroutine LaserFieldWithManyColorsWithVectorInput


  !----------------------------------------------------------------
  !>
  !! This subroutine evaluates the value of a specified function at a specific points specified by the input. 
  subroutine BasicFunctionEvaluationWithSingleInput(FunctionName, input, output, p1, p2, p3, p4, p5)
    implicit none
    character(len=*), intent(in) :: FunctionName
    real(kind=dp), intent(in) :: input
    real(kind=dp), intent(out) :: output
    real(kind=dp), intent(in), optional :: p1
    real(kind=dp), intent(in), optional :: p2
    real(kind=dp), intent(in), optional :: p3
    real(kind=dp), intent(in), optional :: p4
    real(kind=dp), intent(in), optional :: p5

    ! Internal
    real(kind=dp) :: input_tmp(1)
    real(kind=dp) :: output_tmp(1)

    input_tmp(1) = input
    call BasicFunctionEvaluationWithVectorInput(FunctionName, 1, input_tmp, output_tmp, p1, p2, p3, p4, p5)
    output = output_tmp(1)

  end subroutine BasicFunctionEvaluationWithSingleInput


  !----------------------------------------------------------------
  !>
  !! This subroutine evaluates values of a specified function at points specified by input array. 
  !! Let input = x, output =y, and paramters = p1, p2, p3, etc:
  !! Case = constant:
  !!   y = p1
  !! Case = cosine
  !!   y = p1*cos(p2*(x - p4) + p3)
  !! Case = sine
  !!   y = p1*sin(p2*(x - p4) + p3)
  !! Case = sine-quadratic
  !!   y = p1*sin^2(2.d0*pi*(x - p4)/T) ; T = p3 - p2
  !! Case = sine-quartic
  !!   y = p1*sin^4(2.d0*pi*(x - p4)/T) ; T = p3 - p2
  !! Case = gaussian
  !!   y = p1*exp^(-0.5*((x - p2)/p3)^2)
  !! Case = lorentzian
  !!   y = 1/(p1*pi*(1 + ((x - p2)/p1)^2))
  !! Case = ramp-sine-up-constant-down
  !!   y = 0 ; if x < p2
  !!   y = p1*sin(2*pi/T*(x - p2)) , T = 4*(p3 - p2); if p2 <= x < p3
  !!   y = p1 ; if p3 <= x < p4
  !!   y = p1*cos(2*pi/T*(x - p4)) , T = 4*(p5 - p4); if p4 <= x < p5
  !!   y = 0 ; if x >= p5
  !! Case = ramp-sine-up
  !!   y = 0 ; if x < p2
  !!   y = p1*sin(2*pi/T*(x - p2)) , T = 4*(p3 - p2); if p2 <= x < p3
  !!   y = p1 ; if p3 <= x
  !! Case = ramp-sine-down
  !!   y = p1 ; if x < p2
  !!   y = p1*cos(2*pi/T*(x - p2)) , T = 4*(p3 - p2); if p2 <= x < p3
  !!   y = 0 ; if p3 <= x
  !! Case = ramp-gaussian-up-constant-down
  !!   y = p1*exp^(-0.5*((x - p2)/p3)^2) ; if x < p2
  !!   y = p1 ; if p2 <= x < p4
  !!   y = p1*exp^(-0.5*((x - p4)/p5)^2) ; if p4 <= x
  !! Case = ramp-gaussian-up
  !!   y = p1*exp^(-0.5*((x - p2)/p3)^2) ; if x < p2
  !!   y = p1 ; if p2 <= x
  !! Case = ramp-gaussian-down
  !!   y = p1 ; if p2 x < p2
  !!   y = p1*exp^(-0.5*((x - p2)/p3)^2) ; if p2 <= x
  !!   
  subroutine BasicFunctionEvaluationWithVectorInput(FunctionName, InputLength, input, output, p1, p2, p3, p4, p5)
    implicit none
    character(len=*), intent(in) :: FunctionName
    integer, intent(in) :: InputLength 
    real(kind=dp), intent(in) :: input(InputLength)
    real(kind=dp), intent(out) :: output(InputLength)
    real(kind=dp), intent(in), optional :: p1
    real(kind=dp), intent(in), optional :: p2
    real(kind=dp), intent(in), optional :: p3
    real(kind=dp), intent(in), optional :: p4
    real(kind=dp), intent(in), optional :: p5
 
    ! Internal
    integer :: i
    real(kind=dp) :: period1, period2
    
    select case (FunctionName)
      case ('heaviside')
        if(.not.(present(p1) .and. present(p2))) then 
          call MessageText('Error', 'At least one required parameter for the scaled Heaviside function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        output = 0.d0
        where(input >= p2) output = p1
      case ('constant')
        if(.not.present(p1)) then 
          call MessageText('Error', 'An required parameter for the constant function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        output = p1
      case ('cosine')
        if(.not.(present(p1) .and. present(p2) .and. present(p3) .and. present(p4))) then 
          call MessageText('Error', 'At least one required parameter for the cosine function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        output = p1*dcos(p2*(input - p4) + p3)
      case ('sine')
        if(.not.(present(p1) .and. present(p2) .and. present(p3) .and. present(p4))) then 
          call MessageText('Error', 'At least one required parameter for the sine function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        output = p1*dsin(p2*(input - p4) + p3)
      case ('sine-quadratic')
        if(.not.(present(p1) .and. present(p2) .and. present(p3))) then 
          call MessageText('Error', 'At least one required parameter for the cosine function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        period1 = 2.d0*(p3 - p2)
        where(input >= p2 .and. input < p3)
          output = p1*(dsin(2.d0*pi*(input - p2)/period1))**2
        elsewhere
          output = 0.d0
        end where
      case ('sine-quartic')
        if(.not.(present(p1) .and. present(p2) .and. present(p3))) then 
          call MessageText('Error', 'At least one required parameter for the cosine function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        period1 = 2.d0*(p3 - p2)
        where(input >= p2 .and. input < p3)
          output = p1*(dsin(2.d0*pi*(input - p2)/period1))**4
        elsewhere
          output = 0.d0
        end where
      case ('gaussian')
        if(.not.(present(p1) .and. present(p2) .and. present(p3))) then 
          call MessageText('Error', 'At least one required parameter for the Gaussian function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        output = p1*dexp(-0.5d0*((input - p2)/p3)**2.d0)
      case ('lorentzian')
        if(.not.(present(p1) .and. present(p2))) then 
          call MessageText('Error', 'At least one required parameter for the Lorentzian function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        output = 1.d0/(pi*p1)/(1.d0 + ((input - p2)/p1)**2.d0)
      case ('ramp-sine-up-constant-down')
        if(.not.(present(p1) .and. present(p2) .and. present(p3) .and. present(p4) .and. present(p5))) then 
          call MessageText('Error', 'At least one required parameter for the ramp-sine-up-constant-down function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        period1 = 4.d0*(p3 - p2)
        period2 = 4.d0*(p5 - p4)
        where(input >= p2 .and. input <p3) 
          output = p1*dsin(2.d0*pi/period1*(input - p2))
        elsewhere(input >= p3 .and. input < p4)
          output = p1
        elsewhere(input >= p4 .and. input < p5)
          output = p1*dcos(2.d0*pi/period2*(input - p4))
        elsewhere
          output = 0.d0
        end where
      case ('ramp-sine-up')
        if(.not.(present(p1) .and. present(p2) .and. present(p3))) then 
          call MessageText('Error', 'At least one required parameter for the ramp-sine-up function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        period1 = 4.d0*(p3 - p2)
        where(input >= p2 .and. input < p3)
          output = p1*dsin(2.d0*pi/period1*(input - p2))
        elsewhere(input >= p3)
          output = p1
        elsewhere
          output = 0.d0
        end where
      case ('ramp-sine-down')
        if(.not.(present(p1) .and. present(p2) .and. present(p3))) then 
          call MessageText('Error', 'At least one required parameter for the ramp-sine-down function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        period1 = 4.d0*(p3 - p2)
        where(input < p2)
          output = p1
        elsewhere(input >= p2 .and. input < p3)
          output = p1*dcos(2.d0*pi/period1*(input - p2))
        elsewhere
          output = 0.d0
        end where
      case ('ramp-gaussian-up-constant-down')
        if(.not.(present(p1) .and. present(p2) .and. present(p3) .and. present(p4) .and. present(p5))) then 
          call MessageText('Error', 'At least one required parameter for the ramp-gaussian-up-constant-down function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        where(input < p2) 
          output = p1*dexp(-0.5d0*((input - p2)/p3)**2.d0)
        elsewhere(input >= p4)
          output = p1*dexp(-0.5d0*((input - p4)/p5)**2.d0)
        elsewhere
          output = p1
        end where
      case ('ramp-gaussian-up')
        if(.not.(present(p1) .and. present(p2) .and. present(p3))) then 
          call MessageText('Error', 'At least one required parameter for the ramp-gaussian-up function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        where(input < p2)
          output = p1*dexp(-0.5d0*((input - p2)/p3)**2.d0)
        elsewhere
          output = p1
        end where
      case ('ramp-gaussian-down')
        if(.not.(present(p1) .and. present(p2) .and. present(p3))) then 
          call MessageText('Error', 'At least one required parameter for the ramp-gaussian-down function is missing.', 'LaserFieldMDL', 'BasicFunction')
        end if
        where(input >= p2)
          output = p1*dexp(-0.5d0*((input - p2)/p3)**2.d0)
        elsewhere
          output = p1
        end where
      case default
        call MessageText('Error', 'Invalid name of function.', 'LaserFieldMDL', 'BasicFunction')
    end select
  end subroutine BasicFunctionEvaluationWithVectorInput
  

  !----------------------------------------------------------------
  !>
  !! This subroutine is for testing subroutines in this module.
  subroutine LaserFieldMDL_Tester()
    implicit none
    type(LaserParameter), allocatable :: LP(:)
    character(len=99) :: FromThis, ToThis
    integer :: i, n, n_color
    real(kind=dp) :: input
    real(kind=dp), allocatable :: A(:), B(:)
    real(kind=dp) :: p1, p2, p3, p4, p5

    ! Write unit
    integer :: WU

    open(newunit = WU, file = 'LaserFieldMDL_Tester.dat')

    ! test for LaserFieldParamterConversion
    do while (.true.)
      print*, 'Specify the source unit, quantity, and target unit(speciy X to quit this part):'
      read(*, '(A)') FromThis
      if(trim(adjustl(FromThis)) == 'X') exit
      read(*,*) input
      read(*, '(A)') ToThis
      write(*,'(A,X,A,X,A,X,ES)'), 'Convert', trim(adjustl(FromThis)), 'with value', input
      write(*,'(A,X,A,X,ES)') trim(adjustl(ToThis)), 'with value', LaserFieldParameterConversion(FromThis, Input, ToThis)
      print*, ''
    end do

    ! test for BasicFunctionEvaluation
    n = 500
    allocate(A(n), B(n))
    A = 0.1d0*dble((/ (i - n/2, i = 1, n)  /))
    p1 = 5.d0
    p2 = 2.d0
    p3 = 4.d0
    p4 = 10.d0
    p5 = 30.d0
    call BasicFunctionEvaluation('ramp-gaussian-up-constant-down', n, A, B, p1, p2, p3, p4, p5)
    write(WU, *) '# Test: BasicFunctionEvaluation'
    do i = 1, n
      write(WU, *) A(i), B(i)
    end do
    write(WU, *) ''
    write(WU, *) ''

    ! test for LaserField
    n_color = 2 
    allocate(LP(n_color))
    LP(1)%envelope = 'sine-quadratic'
    LP(1)%amplitude = 1.d0
    LP(1)%AngularFrequency = 2.d0
    LP(1)%PhaseShift = 0.d0
    LP(1)%TimeShift = 0.d0
    LP(1)%EnvelopeHeight = 1.d0
    LP(1)%EnvelopeTime1 = 0.d0
    LP(1)%EnvelopeTime2 = 0.d0
    LP(1)%EnvelopeProperty1 = 97.d0
    LP(1)%EnvelopeProperty2 = 3.d0

    LP(2)%envelope = 'constant'
    LP(2)%amplitude = 1.d0
    LP(2)%AngularFrequency = 0.d0
    LP(2)%PhaseShift = 0.d0*pi
    LP(2)%TimeShift = 0.d0
    LP(2)%EnvelopeHeight = 1.d0
    call LaserField(n_color, n, A, LP, B)
    write(WU, *) '# Test: LaserField'
    do i = 1, n
      write(WU, *) A(i), B(i)
    end do
    write(WU, *) ''
    write(WU, *) ''

  end subroutine



  
  !===========
  !Old Subroutines
  !===========
  
subroutine LaserFieldGeneratorNew(n_color,n_time,ColorParameters,field,time)
!This is just a wrapper for the function LaserFieldGeneratorFunction so one can just call this to generate a pulse stored in an array called field.
!===Integer===
!n_color: number of color in the laser. The color is in the form of Envlp(t)*cos(wt-phi). The laser is the total sum of each colors.
!n_time: n_time +1 = number of time grid. 
!===Real Double===
!ColorParameters: see LaserFieldGeneratorFunction for details.
!field(0:n_time): field(i) is the laser field at ith time grid.
!time(0:n_time): time(i) is the time at ith time grid.
implicit none
integer :: n_color, n_time
real(kind=dp) :: ColorParameters(n_color,-100:100), field(0:n_time), time(0:n_time)
!dummy variables
integer :: t
field=0.d0
do t=0,n_time
  field(t)=LaserFieldGeneratorFunction(n_color,ColorParameters,time(t))
enddo
endsubroutine


subroutine LaserFieldGenerator(n_color,n_time,color_parameters,field,time)
!Everything is in atomic unit
!===Integer===
!n_color: number of color in the laser. The color is in the form of Envlp(t)*cos(wt-phi). The laser is the total sum of each colors.
!n_time: n_time +1 = number of time grid. 
!===Real Double===
!color_parameters(n_color,6): this contains the information of specific color.
!                             color_parameters(i,1): the field amplitude of ith color.          
!                             color_parameters(i,2): the wavelength of ith color. If wavelength>=1.d10, frequency is set to zero. This is for generating envelop field.
!                             color_parameters(i,3): the phase of ith color. Useful note: shift in time=period*phase/2pi. Phase is set to zero if wavelength>=1.d10. 
!                             color_parameters(i,4): the envelop for ith color. 1.d0:constant; 2.d0:cosine square; 3.d0:Gaussian.         
!                             color_parameters(i,5): the center of the envelop for ith color.
!                             color_parameters(i,6): the "width" of the envelop for ith color. It has different meaning for various envelop type. constant:not utilized; consin square:extension of cosine square; Gaussain:standard variation.
!field(0:n_time): field(i) is the laser field at ith time grid.
!time(0:n_time): time(i) is the time at ith time grid.
implicit none
integer :: n_color, n_time
real(kind=dp) :: color_parameters(n_color,6), field(0:n_time), time(0:n_time)
!temporary variables
integer :: i, t
real(kind=dp) :: E0, envlp(0:n_time), phi, w
!calculate the field
field=0.d0
envlp=0.d0
do i=1, n_color,1
  E0=color_parameters(i,1)
  w=2.d0*pi*u_LightSpeed/color_parameters(i,2);if(color_parameters(i,2)>=1.d10) w=0.d0
  phi=color_parameters(i,3);if(color_parameters(i,2)>=1.d10) phi=0.d0
  call calculate_envlp(int(color_parameters(i,4)),color_parameters(i,5),color_parameters(i,6))
  do t=0, n_time, 1
    field(t)=field(t)+envlp(t)*E0*dcos(w*time(t)-phi)
  enddo
enddo
!contained subroutines
contains
  subroutine calculate_envlp(envlp_type,center,width)
  implicit none
  integer :: envlp_type
  real(kind=dp) :: center, width
  integer :: t_p
  if(envlp_type==1) then
    do t_p=0, n_time, 1
      if(dabs(time(t_p)-center)<=0.5d0*width) then
        envlp(t_p)=1.d0
      else
        envlp(t_p)=0.d0
      endif
    enddo
  elseif(envlp_type==2) then
    do t_p=0, n_time, 1
      if(dabs(time(t_p)-center)<=0.5d0*width) then
        envlp(t_p)=dcos(pi*(time(t_p)-center)/width)*dcos(pi*(time(t_p)-center)/width)
      else
        envlp(t_p)=0.d0
      endif
    enddo
  elseif(envlp_type==3) then
    do t_p=0, n_time, 1
      envlp(t_p)=dexp(-(time(t_p)-center)*(time(t_p)-center)/2.d0/width/width)
    enddo
  else
    stop "Error!!!M=LaserFieldMDL;S=LaserFieldGenerator;SS=calculate_envlp;==invalid envlp_type"
  endif
  endsubroutine calculate_envlp
endsubroutine


subroutine WindowGenerator(n_time,WindowType,time,tp,window,wp)
!This is just a wrapper for the function WindowGeneratorFunction so one can just call this to generate a window stored in an array called window.
!See the description in WindowGeneratorFunction for actual definitions of variables.
implicit none
integer :: n_time, WindowType
real(kind=8) :: time(0:n_time), tp(50), window(0:n_time), wp(50)
integer :: t
do t=0,n_time
  window(t)=WindowGeneratorFunction(WindowType,time(t),tp,wp)
enddo
endsubroutine





!=========
!Functions
!=========


function LaserFieldGeneratorFunction(n_color,ColorParameters,PresentTime)
!This retrun the electric field at specified Time with given ColorParameters. The color is in the form of Envlp(t)*cos(w(t-t_begin)+phi). The laser is the total sum of each colors..
!Everything is in atomic unit
!===Integer===
!n_color: number of color in the laser.
!===Real Double===
!ColorParameters(n_color,-100:100): this contains the information of specific color. We shorten ColorParameters to cp below.
!  cp(i,0): the type of the laser of ith color. Remember that, although it's shown integer below, this input is double real.
!           0=constant(rectangular) laser field, namely DC field within a time window.
!           1=constant(rectangular)-envelop laser field.
!           2=cosine-square-envelop laser field.
!           3=Gaussian-envelop laser field.
!  cp(i,1): the intensity of ith color.          
!  cp(i,2): the wavelength of ith color.
!  cp(i,3): the phase of ith color. Useful note: shift in time=period*phase/2pi.
!  cp(i,4): t_begin, the beginning of the pulse. Note that you can also treat it as shifting time. Just keep in mind the laser is in the form mentioned in above.
!  cp(i,j): for negative j, the actual definitions change according to the type of the laser for ith color:
!           For cp(i,0)=0:
!             cp(i,-1): the beginning time of the constant(rectangular) envelop for ith color.
!             cp(i,-2): the end time of the constant(rectangular) envelop for ith color.
!           For cp(i,0)=1:
!             cp(i,-1): the beginning time of the constant(rectangular) envelop for ith color.
!             cp(i,-2): the end time of the constant(rectangular) envelop for ith color.
!           For cp(i,0)=2:
!             cp(i,-1): the center of the cosine square envelop for ith color.
!             cp(i,-2): the width of the cosine square envelop for ith color.
!           For cp(i,0)=3:
!             cp(i,-1): the center of the Gaussian envelop for ith color.
!             cp(i,-2): the standard deviation of Gaussian envelop for ith color.
!PresentTime: present time.
implicit none
integer :: n_color
real(kind=8) :: ColorParameters(n_color,-100:100), LaserFieldGeneratorFunction, PresentTime
!temporary variables
integer :: i
real(kind=dp) :: E0, field, phi, t_b, tp(50), w, wp(50)
!calculate the field
tp=0.d0;wp=0.d0;field=0.d0
do i=1, n_color,1
  E0=dsqrt(ColorParameters(i,1))
  w=2.d0*pi*u_LightSpeed/ColorParameters(i,2)
  phi=ColorParameters(i,3)
  t_b=ColorParameters(i,4)
  if(int(ColorParameters(i,0))==0) then
    !only cp(i,-2~1) are used
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(0,PresentTime,tp,wp)
  elseif(int(ColorParameters(i,0))==1) then
    !only cp(i,-2~4) are used
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(0,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==2) then
    !only cp(i,-2~4) are used 
    tp(1)=ColorParameters(i,-1)
    wp(1)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(2,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==3) then
    !only cp(i,1~4), cp(i,-1~-2) are used
    tp(1)=ColorParameters(i,-1)
    wp(1)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(3,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==5) then
    !only cp(i,-2~4) are used 
    tp(1)=ColorParameters(i,-1)
    wp(1)=ColorParameters(i,-2)
    field=field+E0*WindowGeneratorFunction(5,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==6) then ! sine ramp up, constant, and sine ramp down
    !only cp(i,-4~4) are used 
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    tp(3)=ColorParameters(i,-3)
    tp(4)=ColorParameters(i,-4)
    field=field+E0*WindowGeneratorFunction(1,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  elseif(int(ColorParameters(i,0))==7) then ! sine^2 ramp up, constant, and sine^2 ramp down
    !only cp(i,-4~4) are used 
    tp(1)=ColorParameters(i,-1)
    tp(2)=ColorParameters(i,-2)
    tp(3)=ColorParameters(i,-3)
    tp(4)=ColorParameters(i,-4)
    field=field+E0*WindowGeneratorFunction(6,PresentTime,tp,wp)*dcos(w*(PresentTime-t_b)+phi)
  else
    stop "Error!!!M=LaserFieldMDL;F=LaserGeneratorFunction;==Invalid ColorParameter(0)!"
  endif
enddo
LaserFieldGeneratorFunction=field
endfunction LaserFieldGeneratorFunction


function WindowGeneratorFunction(WindowType,present_time,tp,wp)
!This function generates the value of a specified window(or envelop) at specific time present_time. The height of all window is one.
!WindowType: specifyin the type of window.
!present_time: present time.
!tp and wp: reserved data arrays for time and window parameters.
implicit none
integer :: WindowType
real(kind=8) :: present_time, WindowGeneratorFunction, tp(50), wp(50)
real(kind=8) :: r1, r2, r3
if(WindowType==0) then 
!constant window(rectangular window)
!tp1=begin of window
!tp2=end of window
  if((present_time>=tp(1)).and.(present_time<=tp(2))) then
    WindowGeneratorFunction=1.d0
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==1) then
!sine ramp up, constant, sine ramp down window
!tp1=begin of sine ramp up
!tp2=end of sine ramp up=begin of constant window
!tp3=begin of sine ramp down=end of constant window
!tp4=end of sine ramp down
!elements in tp and wp not mentioned above are not referenced
  if(.not.( (tp(1)<tp(2)).and.(tp(2)<=tp(3)).and.(tp(3)<tp(4)) )) stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid values for array tp for WindowType=1!"
  r1=pi/(2.d0*(tp(2)-tp(1)))!radian frequency for sine of ramp up
  r2=pi/(2.d0*(tp(4)-tp(3)))!randian frequency for cosine of ramp down
  if((present_time>=tp(1)).and.(present_time<tp(2))) then
    WindowGeneratorFunction=dsin(r1*(present_time-tp(1)))
  elseif((present_time>=tp(2)).and.(present_time<tp(3))) then
    WindowGeneratorFunction=1.d0
  elseif((present_time>=tp(3)).and.(present_time<=tp(4))) then
    WindowGeneratorFunction=dcos(r2*(present_time-tp(3)))
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==2) then
!cosine square window
!tp1=center of the window
!wp1=width of the window
  r1=tp(1) !center of the cosine square
  r2=wp(1) !width of the window
  r3=pi/r2 !radian frequency of cosine square
  if(dabs(present_time-r1)<=0.5d0*r2) then
    WindowGeneratorFunction=dcos(r3*(present_time-r1))*dcos(r3*(present_time-r1))
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==3) then
!Gaussian window (not the normalized Gaussian, the value of Gaussian is one at the center of the Gaussian)
!tp1=center of the Gaussian
!wp1=standard deviation of Gaussian
  r1=tp(1) !center of Gaussian
  r2=wp(1) !standard deviation of Gaussian
  WindowGeneratorFunction=dexp(-(present_time-r1)*(present_time-r1)/2.d0/r2/r2)
elseif(WindowType==4) then
!polynomially decay window
!1-3x^2+2x^3  and 1 before beginning and zero after this decay
!tp1=beginning of the decay
!tp2=end of the decay
  if((tp(2)-tp(1))<=0) stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid values for array tp for WindowType=4!"
  r1=tp(2)-tp(1)
  if((present_time>=tp(1)).and.(present_time<=tp(2))) then
    WindowGeneratorFunction=1.d0-3.d0*((present_time-tp(1))/r1)**2.d0+2.d0*((present_time-tp(1))/r1)**3.d0
  elseif(present_time<tp(1)) then 
    WindowGeneratorFunction=1.d0
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==5) then
!sine^4 window
!tp1=center of the window
!tp2=width of the window
  r1=tp(1) !center of the cosine^4
  r2=wp(1) !width of the window
  r3=pi/r2 !radian frequency of cosine square
  if(dabs(present_time-r1)<=0.5d0*r2) then
    WindowGeneratorFunction=dcos(r3*(present_time-r1))**4.d0
  else
    WindowGeneratorFunction=0.d0
  endif
elseif(WindowType==6) then
!sine^2 ramp up, constant, sine^2 ramp down window
!tp1=begin of sine ramp up
!tp2=end of sine^2 ramp up=begin of constant window
!tp3=begin of sine^2 ramp down=end of constant window
!tp4=end of sine ramp down
!elements in tp and wp not mentioned above are not referenced
  if(.not.( (tp(1)<tp(2)).and.(tp(2)<=tp(3)).and.(tp(3)<tp(4)) )) stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid values for array tp for WindowType=6!"
  r1=pi/(2.d0*(tp(2)-tp(1)))
  r2=pi/(2.d0*(tp(4)-tp(3)))
  if((present_time>=tp(1)).and.(present_time<tp(2))) then
    WindowGeneratorFunction=dsin(r1*(present_time-tp(1)))**2
  elseif((present_time>=tp(2)).and.(present_time<tp(3))) then
    WindowGeneratorFunction=1.d0
  elseif((present_time>=tp(3)).and.(present_time<=tp(4))) then
    WindowGeneratorFunction=dcos(r2*(present_time-tp(3)))**2
  else
    WindowGeneratorFunction=0.d0
  endif
else
  stop "Error!!!M=LaserFieldMDL;F=WindowGeneratorFunction;==Invalid WindowType!"
endif
endfunction


ENDMODULE LaserFieldMDL



