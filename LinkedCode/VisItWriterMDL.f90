!Note that the idea of "frame" is used almost in every writer. 
!It's about how many data points we actaully use from the given data.
!This idea can be understood by a simple example. 
!Say I have 5 data points, but I just want to plot 3 of them. 
!Then by assigning "frame" to 3, we take data point 1, 3, and 5. 
!By default the first and last data point will always be taken. 
!This idea of frame can make data writing more simpler since generally plotting the whole data make the ouput a huge file and slow down the plotting process.



MODULE VisItWriterMDL
implicit none
integer, parameter :: dp=kind(1.d0)

private dp

contains


subroutine WriteDataCurve2D_DoubleReal(WU,title,nf,curve,x,unit_y,unit_x,order_switch,n)
!Remember you can call this in series for the same write unit to write many data in the same file with corresponding order. However, rember to use different title to distinquich them in the VisIt.
!
!===Parameters===
!WU: the unit of file for writer in fortran.
!title: the title of the data. This is writtern at the first line of the file.
!nf: number of "frame" taken from the input data. 
!curve: the data for the curve in the y axis.
!x: the data for the curve in the x axis.
!unit_y: the unit in y axis. 
!unit_x: the unit in x axis.
!order switch: this is used to invert the role of x and y. Set it to zero so curve and x are plot at vertical and horizontal axis, respectively. Otherwise curve and x will be plot in horizontal and vertical axis, respectively. 
!
implicit none
integer, intent(in) :: WU, n, nf, order_switch !nf is the number of the frame
character(len=*), intent(in) :: title
real(dp), intent(in) :: curve(n), unit_x, unit_y, x(n)
integer :: i, fi(nf)
real(kind=8) :: step
if(nf<n) then 
  step=dble(n)/dble(nf-1)
elseif(nf==n) then
  step=1.d0
else
  stop "Error!!!M=VisItWriterMDL;S=WriteDataCurve2D;==number of frame is greater than the data points available!"
endif
do i=1,nf-1
  fi(i)=nint(dble(i-1)*step)+1
enddo
fi(nf)=n
write(WU,*) "#", title
if(order_switch==0) then
  do i=1,nf
    write(WU,*) x(fi(i))/unit_x, curve(fi(i))/unit_y
  enddo
else
  do i=1,nf
    write(WU,*) curve(fi(i))/unit_y, x(fi(i))/unit_x
  enddo
endif
write(WU,*) ""
write(WU,*) ""
endsubroutine


subroutine WriteDataVTK_2D_Density_DoubleReal(WU,title,density,nfx,nfy,x,y,nx,ny,unit_x,unit_y)
!WU: the unit of file for writer in fortran
!title: the title of the data.
!density: the 2D array for the density data.
!nfx: the number of "frame" taken from the input data in the x axis.
!nfy: the number of "frame" taken from the input data in the y axis.
!x: the coordinate in the x axis.
!y: the coordinate in the y axis.
!nx: size of x array.
!ny: size of y array.
!unit_x: the unit in x axis.
!unit_y: the unit in y axis.
implicit none
integer :: WU, nfx, nfy, nx, ny
character(len=*), intent(in) :: title 
real(dp) :: density(nx,ny),  x(nx), y(ny), unit_x, unit_y
integer :: idx(nfx), idy(nfy), ix, iy
real(kind=8) :: step_x, step_y
character*3 :: title_format
if(nfx<nx) then
  step_x=dble(nx)/dble(nfx-1)
elseif(nfx==nx) then
  step_x=1.d0
else
  stop "Error!!!M=VisItWriter;S=WriteDataVTK_DoubleReal;==number of frame in x is greater than the available data points!"
endif
if(nfy<ny) then
  step_y=dble(ny)/dble(nfy-1)
elseif(nfy==ny) then
  step_y=1.d0
else
  stop "Error!!!M=VisItWriter;S=WriteDataVTK_DoubleReal;==number of frame in y is greater than the available data points!"
endif
do ix=1,nfx
  idx(ix)=nint(dble(ix-1)*step_x)+1
enddo
idx(nfx)=nx
do iy=1,nfy
  idy(iy)=(iy-1)*step_y+1
enddo
idy(nfy)=ny
write(WU,"(A                  )") "# vtk DataFile Version 3.0"
write(WU,"(A                  )") title
write(WU,"(A                  )") "ASCII"
write(WU,"(A                  )") "DATASET RECTILINEAR_GRID"
write(WU,"(A,1X,I,1X,I,1X,I   )") "DIMENSIONS", nfx, nfy, 1
write(WU,"(A,1X,I,1X          )") "X_COORDINATES", nfx," FLOAT"
do ix=1, nfx
  write(WU,*) x(idx(ix))/unit_x
enddo
write(WU,"(A,1X,I,1X,A        )") "Y_COORDINATES", nfy, " FLOAT"
do iy=1,nfy
  write(WU,*) y(idy(iy))/unit_y
enddo
write(WU,"(A,1X,I,1X,A        )") "Z_COORDINATES", 1, " FLOAT"
  write(WU,*) 0.d0
write(WU,"(A,1X,I             )") "POINT_DATA ", nfx*nfy
write(WU,"(A                  )") "SCALARS density float"
write(WU,"(A                  )") "LOOKUP_TABLE default"
do iy=1,nfy
  do ix=1,nfx
    write(WU,"(F15.11)") density(idx(ix),idy(iy))
  enddo
enddo
endsubroutine


function DataReturnTotalNumberOfLine(n_empty_read,WU)
implicit none
integer, intent(in) :: n_empty_read, WU
integer :: DataReturnTotalNumberOfLine
!internal
integer :: i, IOinfo
real(kind=dp) :: dummy
if(n_empty_read.ne.0) then
  do i=1,n_empty_read
    read(WU, *) 
  enddo
endif
DataReturnTotalNumberOfLine=0
do while(1)
  read(WU,*,IOSTAT=IOinfo) dummy
  if(IOinfo<0) exit
  DataReturnTotalNumberOfLine=DataReturnTotalNumberOfLine+1
enddo
endfunction


subroutine DataReading(n_data_column,n_data_row,n_empty_read,WU,M) !now only support data of just 1 or 2 column
implicit none
integer, intent(in) :: n_data_column, n_data_row, n_empty_read, WU
real(kind=dp), intent(out) :: M(n_data_row,n_data_column)
!internal
integer :: i
if(n_empty_read.ne.0) then
  do i=1,n_empty_read
    read(WU, *) 
  enddo
endif
do i=1,n_data_row
  if(n_data_column==1) then
    read(WU,*) M(i,1)
  elseif(n_data_column==2) then
    read(WU,*) M(i,1), M(i,2)
  else
    stop "Error!!!M=VisItWriter;S=DataReading;==Now only data with at most 2 columns is supported!"
  endif
enddo
endsubroutine


ENDMODULE VisItWriterMDL




