program ClassicalTrajectoryAnalysis
use UnitAndConstantMDL
use GlobalMDL
implicit none

print*, 'Program starts.'
print*, '===================================='

call Initialization(band, field, ScatterPointAtBand, sys)


print*, 'TD starts at:'
call EXECUTE_COMMAND_LINE('date')
print*, '========================='

call Propagation(sys%HighestBandExaminedForScatter, band, field, ScatterPointAtBand, sys)

print*, 'TD ends at:'
call EXECUTE_COMMAND_LINE('date')
print*, '========================='

print*, 'Calculation is completed and the program ends.'
print*, '===================================='

end program
