module GlobalMDL
use UnitAndConstantMDL
use LaserFieldMDL
use VisItWriterMDL
use InputOutputMDL
implicit none

type ScatterMemory_t
  integer :: TimeGrid
  integer :: AccumulatedNumberOfScattering ! number of scattering occured before this branch point.
  integer :: AccumulatedNumberOfRecombination ! number of recombination occured before this branch 
                                              !   point(not including the one righ on the 
                                              !   scattering time if recombination also happens).
  integer :: band(0:1) ! 0 and 1 for non-scattered and scattered bands.
  integer :: state  ! 0 and 1 for non-scattered and scattered paths.
  real(kind=dp) :: AbsoluteRelativeDistance
  real(kind=dp) :: k
  real(kind=dp) :: x
end type

type RecombinationMemory_t
  integer :: NumberOfRecombination
  integer :: NumberOfScattering
  real(kind=dp) :: Energy
  real(kind=dp) :: TimeExcitation
  real(kind=dp) :: TimeRecombination
end type

type RecombinationMemoryForAllField_t
  integer :: NumberOfRecombination
  type(RecombinationMemory_t), allocatable :: RM(:) ! recombination memory
end type

type ScatterPoint_t
  ! Together with a declared variable of this type using the 
  !   name "ScatterPointAtBand", the ith scatter point in the 
  !   nth band is designated by "ScatterPointAtBand(n)%AtPoint(i)".
  !   And the switch for the scattering is designated as 
  !   "ScatterPointAtBand(n)%IsAllowed". Also, the allowed 
  !   band index change at the scattering point is designated by
  !   "ScatterPointAtBand(n)%AtPointToward(i)", which is either
  !   +1 or -1(band climb or descend). Keep in mind we assume 
  !   electron to remain in the band by default.
  logical :: IsAllowed
  integer :: NumberOfPoint
  integer, allocatable :: AtPointToward(:) ! direction at this point for the non-scattered path
  real(kind=dp), allocatable :: AtPoint(:)
end type

type band_t
  integer :: BandChoice
  integer :: NumberOfBand
  integer :: MaxFourierComponent
  real(kind=dp) :: ReciprocalLatticeVector
  real(kind=dp) :: gap
  real(kind=dp) :: Meff
  real(kind=dp) :: LinearBandGroupVelocity
  real(kind=dp), allocatable :: FourierComponent(:,:) 
end type

type field_t
  integer :: FieldChangingParameter
  integer :: FieldChoice
  integer :: n_field
  real(kind=dp) :: amplitude
  real(kind=dp) :: lambda
  real(kind=dp) :: omega
  real(kind=dp) :: period
  real(kind=dp) :: E0_min
  real(kind=dp) :: E0_max
  real(kind=dp) :: lambda_min
  real(kind=dp) :: lambda_max
end type

type system_t
  logical :: ScatterIsAllowed
  logical :: IsMeanFreePathConstraintApplied
  integer :: BandChoice
  integer :: FieldChoice
  integer :: MaximumReturnRecorded
  integer :: n_scatter_max
  integer :: HighestBandExaminedForScatter
  integer :: MaximumNumberOfBranchForScatter
  integer :: n_trj
  integer :: n_field
  real(kind=dp) :: LatticeConstant
  real(kind=dp) :: tolerance_k
  real(kind=dp) :: tolerance_t
  real(kind=dp) :: dt
  real(kind=dp) :: gap
  real(kind=dp) :: Meff
  real(kind=dp) :: lambda
  real(kind=dp) :: omega
  real(kind=dp) :: period
  real(kind=dp) :: E0_min
  real(kind=dp) :: E0_max
  real(kind=dp) :: T_Prop_0
  real(kind=dp) :: T_Prop_max
  real(kind=dp) :: TotalPropagationTimeForOneExcitedTime
  real(kind=dp) :: k0
  real(kind=dp) :: MeanFreeLength
  real(kind=dp) :: PonderomotiveEnergyMin
  real(kind=dp) :: PonderomotiveEnergyMax
end type 

type(band_t) :: band
type(field_t) :: field
type(ScatterPoint_t), allocatable :: ScatterPointAtBand(:)
type(system_t) :: sys


public band, field, ScatterPointAtBand, sys
public Initialization, Propagation

contains 

  !----------------------------------------------------------------
  real(dp) function BandEnergy(band, n, k)
    implicit none
    type(band_t), intent(in) :: band
    integer, intent(in) :: n
    real(kind=dp), intent(in) :: k

    ! Internal
    integer :: i
    real(kind=dp) :: kk
    real(kind=dp) :: LatticeConstant

    LatticeConstant = 2.d0*pi/band%ReciprocalLatticeVector

    ! Convert input crystal moemntum into the one in first BZ
    kk = mod(k, band%ReciprocalLatticeVector)
    if(kk >= 0.5d0*band%ReciprocalLatticeVector) then 
      kk = kk - band%ReciprocalLatticeVector
    elseif(kk <= -0.5d0*band%ReciprocalLatticeVector) then
      kk = kk + band%ReciprocalLatticeVector
    end if

    ! Return band energy for differnt choice of band structures
    select case (band%BandChoice)
    case (1) ! The first band is chosen to be a flat band
      if (n == 1) then
        BandEnergy = 0.d0
      elseif (mod(n, 2) == 0) then
        BandEnergy = 0.5d0/band%Meff*(kk + sign(1.d0, kk)*dble(n/2 - 1  )*band%ReciprocalLatticeVector)**2.d0 + band%gap
      else
        BandEnergy = 0.5d0/band%Meff*(kk - sign(1.d0, kk)*dble((n - 1)/2)*band%ReciprocalLatticeVector)**2.d0 + band%gap
      end if
    case (2) ! The first band is chosen to be a flat band
      if (n == 1) then
        BandEnergy = 0.d0
      elseif ( mod(n, 2) == 0) then
        BandEnergy = band%gap*dsqrt(1.d0 + (kk + sign(1.d0, kk)*dble(n/2 - 1  )*band%ReciprocalLatticeVector)**2.d0/band%gap/band%Meff)
      else
        BandEnergy = band%gap*dsqrt(1.d0 + (kk - sign(1.d0, kk)*dble((n - 1)/2)*band%ReciprocalLatticeVector)**2.d0/band%gap/band%Meff)
      end if
    case (3) ! The first band is chosen to be a flat band
      if (n == 1) then
        BandEnergy = 0.d0
      elseif ( mod(n, 2) == 0) then
        BandEnergy = band%LinearBandGroupVelocity*sign(1.d0, kk)*(kk + sign(1.d0, kk)*dble(n/2 - 1)*band%ReciprocalLatticeVector)
      else
        BandEnergy = -band%LinearBandGroupVelocity*sign(1.d0, kk)*(kk - sign(1.d0, kk)*dble((n - 1)/2)*band%ReciprocalLatticeVector)
      end if
    case (4) ! Read the band structure from the file
      BandEnergy = 0.d0
      do i = 0, band%MaxFourierComponent
        if(i == 0) then 
          BandEnergy = BandEnergy + band%FourierComponent(n,0)
        else
          BandEnergy = BandEnergy + band%FourierComponent(n, i)*2.d0*cos( dble(i)*LatticeConstant*kk )
        end if
      end do
    case default
      call MessageText('Error','S:BandEnergy:: Invalid BandChoice.')
    end select

  end function BandEnergy


  !----------------------------------------------------------------
  real(dp) function GroupVelocity(band, band_present, k)
    implicit none
    type(band_t), intent(in) :: band
    integer, intent(in) :: band_present
    real(kind=dp), intent(in) :: k

    ! Internal
    integer :: shift
    real(kind=dp) :: k_fold
    real(kind=dp) :: k_tmp

    ! Convert input crystal moemntum into the one in first BZ
    k_fold = mod(k, band%ReciprocalLatticeVector)
    if(k_fold >= 0.5d0*band%ReciprocalLatticeVector) then 
      k_fold = k_fold - band%ReciprocalLatticeVector
    elseif(k_fold <= -0.5d0*band%ReciprocalLatticeVector) then
      k_fold = k_fold + band%ReciprocalLatticeVector
    end if

    ! Return the group velocity
    select case(band%BandChoice)
    case (1)
      select case(band_present)
      case (1)
        GroupVelocity = 0.d0
      case default
        select case(mod(band_present,2))
        case (0)
          shift = -int(sign(1.d0, k_fold))*(band_present - 2)/2
        case default
          shift = int(sign(1.d0, k_fold))*((band_present - 1)/2 )
        end select
        k_tmp = k_fold - dble(shift)*band%ReciprocalLatticeVector
        GroupVelocity = k_tmp/band%Meff
      end select
    case (2)
      select case(band_present)
      case (1)
        GroupVelocity = 0.d0
      case default
        select case(mod(band_present,2))
        case (0)
          shift = -int(sign(1.d0, k_fold))*(band_present - 2)/2
        case default
          shift = int(sign(1.d0, k_fold))*((band_present - 1)/2)
        end select
        k_tmp = k_fold - dble(shift)*band%ReciprocalLatticeVector
        GroupVelocity = k_tmp/dsqrt(band%Meff**2 + k_tmp*k_tmp*band%Meff/band%gap)
      end select
    case (3)
      ! Note that the group velocity at the discontinuity is evaluated at 0+ due tp the definition of the "sign" function.
      select case(band_present)
      case (1)
        GroupVelocity = 0.d0
      case default
        select case(mod(band_present,2))
        case (0)
          GroupVelocity = band%LinearBandGroupVelocity*sign(1.d0, k_fold)
        case default
          GroupVelocity = -band%LinearBandGroupVelocity*sign(1.d0, k_fold)
        end select
        GroupVelocity = sign(1.d0, k)*band%LinearBandGroupVelocity
      end select
    case (4)
      call MessageText('Error','S:GroupVelocity:: Currently group velocity calcuation for band selection = 3 is not supported.')
    case default
      call MessageText('Error','S:GroupVelocity:: Invalid BandChoice.')
    end select

  end function GroupVelocity

 
  !----------------------------------------------------------------
  real(dp) function SSsFormulaForCutOff(angular_frequency, effective_mass, field_amplitude, Gap)
    implicit none
    real(kind=dp), intent(in) :: angular_frequency
    real(kind=dp), intent(in) :: effective_mass
    real(kind=dp), intent(in) :: field_amplitude
    real(kind=dp), intent(in) :: Gap

    ! Internal
    real(kind=dp) :: U ! ponderomotive energy/gap

    U = field_amplitude * field_amplitude /4.d0/effective_mass/angular_frequency/angular_frequency/Gap

    SSsFormulaForCutOff = Gap * dsqrt(1.d0 + 2.d0*3.17d0*U*((1.d0 + 1.44d0*U)/(1.d0 + U)))

  end function SSsFormulaForCutOff

  !----------------------------------------------------------------
  subroutine Initialization(band, field, ScatterPointAtBand, sys)
    implicit none
    type(band_t), intent(out) :: band
    type(field_t), intent(out) :: field
    type(ScatterPoint_t), allocatable, intent(out) :: ScatterPointAtBand(:)
    type(system_t), intent(out) :: sys
    character(len=*), parameter :: DefaultNameForBandReadInFile = 'BandReadInFile.in'

    ! Internal
    integer, parameter :: NumberOfPointForBandPlotting = 400
    integer, parameter :: LargestBrillouinZoneConsidered = 10
    integer :: i, j
    real(kind=dp) :: k
    
    ! WU
    integer :: WU_BandPlot, WU_BandReadInFile

    call Parse('BandChoice', band%BandChoice)
    call Parse('LatticeConstant', sys%LatticeConstant)
    call Parse('dt', sys%dt)
    call Parse('gap', band%gap)
    call Parse('EffectiveMass', band%Meff)
    call Parse('NumberTrajectory', sys%n_trj)
    call Parse('NumberField', field%n_field)
    call Parse('FieldChoice', field%FieldChoice)
    call Parse('lambda', field%lambda)
    call Parse('FieldChangingParameter', field%FieldChangingParameter)
    call Parse('FieldAmplitudeMin', field%E0_min)
    call Parse('FieldAmplitudeMax', field%E0_max)
    call Parse('FieldWavelengthMin', field%lambda_min)
    call Parse('FieldWavelengthMax', field%lambda_max)
    call Parse('PropTimeInitial', sys%T_Prop_0)
    call Parse('PropTimeFinal', sys%T_Prop_max) 
    call Parse('TotalPropagationTimeForOneExcitedTime', sys%TotalPropagationTimeForOneExcitedTime)
    sys%MaximumReturnRecorded = 10 ! this option is erased in the inp.inp file
    sys%HighestBandExaminedForScatter = 20 ! implicitly assigned here
    sys%MaximumNumberOfBranchForScatter = 1000 ! implicitly assigned here; I assumed it >= 10.
    call Parse('InitialK', sys%k0)
    call Parse('ScatterOn', sys%ScatterIsAllowed)
    call Parse('MaximumNumberOfScattering', sys%n_scatter_max)
    call Parse('MeanFreePathConstraintOn', sys%IsMeanFreePathConstraintApplied)
    call Parse('MeanFreePathLength', sys%MeanFreeLength)
    
    call Parse('BandReadInNumberOfBand', band%NumberOfBand)
    call Parse('BandReadInMaxFourierComponent', band%MaxFourierComponent)

    ! this part should be removed if substitution for sys is completed
    call Parse('BandChoice', sys%BandChoice)
    call Parse('gap', sys%gap)
    call Parse('EffectiveMass', sys%Meff)
    call Parse('NumberField', sys%n_field)
    call Parse('FieldChoice', sys%FieldChoice)
    call Parse('lambda', sys%lambda)
    call Parse('FieldAmplitudeMin', sys%E0_min)
    call Parse('FieldAmplitudeMax', sys%E0_max)

    band%ReciprocalLatticeVector = 2.d0*pi/sys%LatticeConstant
    field%lambda = field%lambda*u_NanoMeter
    sys%dt = sys%dt*u_FemtoSecond
    band%gap = band%gap*u_eV
    field%lambda_min = field%lambda_min*u_NanoMeter
    field%lambda_max = field%lambda_max*u_NanoMeter
    field%E0_min = field%E0_min*u_VoltOverAngstrom
    field%E0_max = field%E0_max*u_VoltOverAngstrom
    sys%T_Prop_0 = sys%T_Prop_0*u_FemtoSecond
    sys%T_Prop_max = sys%T_Prop_max*u_FemtoSecond
    sys%TotalPropagationTimeForOneExcitedTime = sys%TotalPropagationTimeForOneExcitedTime*u_FemtoSecond
    sys%tolerance_t = sys%dt*sys%tolerance_t/100.d0
    sys%tolerance_k = 2.d0*pi/sys%LatticeConstant*sys%tolerance_k/100.d0
    field%period = field%lambda_min/u_LightSpeed
    field%omega = 2.d0*pi/field%period
    sys%MeanFreeLength = sys%MeanFreeLength*u_NanoMeter
    sys%PonderomotiveEnergyMin = ((field%E0_min*field%lambda_min/u_LightSpeed/2.d0/pi)**2)/4.d0/band%Meff
    sys%PonderomotiveEnergyMax = ((field%E0_max*field%lambda_max/u_LightSpeed/2.d0/pi)**2)/4.d0/band%Meff
    if(band%BandChoice == 3) Band%LinearBandGroupVelocity = sqrt(band%gap/band%Meff) ! use the input effective mass and band gap to calculate the group velocity(assuming linear band = kane's band at infinity')

    ! this part should be removed if substitution for sys is completed
    sys%lambda = sys%lambda*u_NanoMeter
    sys%gap = sys%gap*u_eV
    sys%E0_min = sys%E0_min*u_VoltOverAngstrom
    sys%E0_max = sys%E0_max*u_VoltOverAngstrom
    sys%period = sys%lambda/u_LightSpeed
    sys%omega = 2.d0*pi/sys%period

    ! Read band structure from file if one chooses to
    if(band%BandChoice == 3) then
      open(newunit = WU_BandReadInFile, file = DefaultNameForBandReadInFile)
      allocate(band%FourierComponent(Band%NumberOfBand, 0:band%MaxFourierComponent))
      read(WU_BandReadInFile, *)
      read(WU_BandReadInFile, *)
      do i = 1, Band%NumberOfBand
        read(WU_BandReadInFile, *)  Band%FourierComponent(i,:)
      end do
    end if

    ! Set up scattering points
    allocate(ScatterPointAtBand(sys%HighestBandExaminedForScatter))
    ScatterPointAtBand(:)%IsAllowed = .false.
    if(sys%ScatterIsAllowed) then
      ! Currently we only consider zone replica up to the third Brillouin zone.
      !   For example, a scatter point at crystal momentum = 0 will be evoke 
      !   a scatter at crystal momentum = +-2g, +-g, and 0.
      select case(band%BandChoice)
      case (1, 2, 3) ! For parabolic, Kane's, and linear band(asymptotic Kane' band at infinity) 
        ! Check if the number of bands considered for scattering is 
        !   above 4(otherwise this explicitly-coded designation will fail).
        if(sys%HighestBandExaminedForScatter < 4) then
          call MessageText('Error','S:Initialization:: Number of bands examined for scattering&
                                                      & should >= 4 for hard-coded point designation.')
        end if
        ! Scattering at band 2 at 0.5 and -0.5g
        i = 2
        ScatterPointAtBand(i)%IsAllowed = .true.
        ScatterPointAtBand(i)%NumberOfPoint = LargestBrillouinZoneConsidered*2  ! 2 edge points x N zones 
        allocate(ScatterPointAtBand(i)%AtPoint(ScatterPointAtBand(i)%NumberOfPoint))
        allocate(ScatterPointAtBand(i)%AtPointToward(ScatterPointAtBand(i)%NumberOfPoint))
        do j = 1, ScatterPointAtBand(i)%NumberOfPoint
          ScatterPointAtBand(i)%AtPoint(j)  = (-(dble(LargestBrillouinZoneConsidered) - 0.5d0) + dble(j - 1))*band%ReciprocalLatticeVector
        end do
        ScatterPointAtBand(i)%AtPointToward = 1
        ! Scattering at band 3 at 0.5, 0, and -0.5g
        i = 3
        ScatterPointAtBand(i)%IsAllowed = .true.
        ScatterPointAtBand(i)%NumberOfPoint = LargestBrillouinZoneConsidered*4 - 1 ! 2 edge points x N zones + 1 non-edge point x ((N zone -1)x2+1)
        allocate(ScatterPointAtBand(i)%AtPoint(ScatterPointAtBand(i)%NumberOfPoint))
        allocate(ScatterPointAtBand(i)%AtPointToward(ScatterPointAtBand(i)%NumberOfPoint))
        do j = 1, ScatterPointAtBand(i)%NumberOfPoint
          ScatterPointAtBand(i)%AtPoint(j)  = (-(dble(LargestBrillouinZoneConsidered) - 0.5d0) + 0.5d0*dble(j - 1))*band%ReciprocalLatticeVector
        end do
        ScatterPointAtBand(i)%AtPointToward(1:ScatterPointAtBand(i)%NumberOfPoint:2) = -1
        ScatterPointAtBand(i)%AtPointToward(2:ScatterPointAtBand(i)%NumberOfPoint-1:2) = 1
        ! The setup for scatter points from band 3 and above are the same except direction for band change.
        do i = 4, sys%HighestBandExaminedForScatter
          ScatterPointAtBand(i)%IsAllowed = .true.
          ScatterPointAtBand(i)%NumberOfPoint = LargestBrillouinZoneConsidered*4 - 1 ! 2 edge points x 3 zones + 1 non-edge point x ((3 zone -1)x2+1)
          allocate(ScatterPointAtBand(i)%AtPoint(ScatterPointAtBand(i)%NumberOfPoint))
          allocate(ScatterPointAtBand(i)%AtPointToward(ScatterPointAtBand(i)%NumberOfPoint))
          ScatterPointAtBand(i)%AtPoint(:)  = ScatterPointAtBand(3)%AtPoint(:)
          ScatterPointAtBand(i)%AtPointToward(:) = ((-1)**(i-3))*ScatterPointAtBand(3)%AtPointToward(:)
        end do
      case (4)
        print*, 'No scattering point set up for this band structure!'
        stop
      case default
        call MessageText('Error','S:Initialization:: Invalid BandChoice.')
      end select
    end if

    ! Plot the band structure
    open(unit = WU_BandPlot, file = 'band.dat')
    write(WU_BandPlot, '(A)') '# Normalized Crystal Momentum, Band Energy(eV)'
    do i = 1, band%NumberOfBand
      do j = 1, NumberOfPointForBandPlotting
        k = -1.5d0 + 3.d0/dble(NumberOfPointForBandPlotting - 1)*dble(j - 1)
        write(WU_BandPlot, *) k, BandEnergy(band, i, k*band%ReciprocalLatticeVector)/u_eV
      end do
      write(WU_BandPlot, *) ''
      write(WU_BandPlot, *) ''
    end do

    ! Write information for the calculation
    write(*,'(A)') '---Basic---'
    write(*,'(A,1X,I)')      'Band choice:                       ', band%BandChoice
    if(band%BandChoice == 4) then
      write(*,'(A,1X,I)')      'Number of band read:                      ', band%NumberOfBand
      write(*,'(A,1X,I)')      'Number of Fourier components of read band:', band%MaxFourierComponent + 1
    end if
    write(*,'(A,1X,F,1X,A)') 'Lattice constant:                  ', sys%LatticeConstant, 'au.'
    write(*,'(A,1X,F,1X,A)') 'Tolerance in k:                    ', sys%tolerance_k*100.d0/2.d0/pi*sys%LatticeConstant, '% of unit reciprocal lattice.'
    write(*,'(A,1X,F,1X,A)') 'Tolerance in time:                 ', sys%tolerance_t/sys%dt*100.d0, '% of dt.'
    write(*,'(A,1X,F,1X,A)') 'Time step:                         ', sys%dt/u_FemtoSecond, 'fs.'
    write(*,'(A,1X,F,1X,A)') 'Band gap:                          ', band%gap/u_eV, 'eV.'
    write(*,'(A,1X,F,1X,A)') 'Effective mass:                    ', band%Meff, 'au'
    write(*,'(A,1X,I)')      'Number of trajectory:              ', sys%n_trj
    write(*,'(A,1X,I)')      'Number of field:                   ', field%n_field
    write(*,'(A,1X,I)')      'Type of the field:                 ', field%FieldChoice
    write(*,'(A,1X,F,1X,A)') 'Wavelength of the field:           ', field%lambda/u_NanoMeter, 'nm.'
    write(*,'(A,1X,F,1X,A)') 'Angular frequency of the field:    ', 2.d0*pi/field%period, 'au.'
    write(*,'(A,1X,F,1X,A)') 'Period of the field:               ', field%period/u_FemtoSecond, 'fs.'
    write(*,'(A,1X,F,1X,A)') 'Minimum electric field amplitude:  ', field%E0_min/u_VoltOverAngstrom, 'V/Angstrom.'
    write(*,'(A,1X,F,1X,A)') 'Maximum electric field amplitude:  ', field%E0_max/u_VoltOverAngstrom, 'V/Angstrom.'
    write(*,'(A,1X,F,1X,A)') 'Minimum Ponderomotive Energy:      ', sys%PonderomotiveEnergyMin/u_eV, 'eV.'
    write(*,'(A,1X,F,1X,A)') 'Maximum Ponderomotive Energy:      ', sys%PonderomotiveEnergyMax/u_eV, 'eV.'
    write(*,'(A,1X,F,1X,A)') 'Initial propagation time:          ', sys%T_Prop_0/u_FemtoSecond, 'fs.'
    write(*,'(A,1X,F,1X,A)') 'Maximum propagation time:          ', sys%T_Prop_max/u_FemtoSecond, 'fs.'
    write(*,'(A,1X,I,1X,A)') 'Maximum return recorded in output: ', sys%MaximumReturnRecorded, 'time(s).'
    write(*,'(A,1X,F,1X,A)') 'Initial crystal momentum:          ', sys%k0/2.d0/pi*sys%LatticeConstant, '(unit reciprocal lattice).'

    write(*,'(A)') '---Scattering---'
    write(*,'(A,1X,L)') 'Scattering is allowed:                  ', sys%ScatterIsAllowed
    write(*,'(A,1X,I,1X,A)') 'Maximum scattering event accounted:', sys%n_scatter_max, 'time(s).'
    write(*,'(A,1X,F,1X,A,1X,F,1X,A)') 'Maximum travel in BZ:    ', field%E0_min*field%lambda_min*sys%LatticeConstant/4.d0/pi/pi/u_LightSpeed, 'to', &
                                                                   &field%E0_max*field%lambda_max*sys%LatticeConstant/4.d0/pi/pi/u_LightSpeed, 'zones'

    write(*,'(A)') '---Path Constraint---'
    write(*,'(A,1X,L)') 'Path constraint is applied:             ', sys%IsMeanFreePathConstraintApplied
    write(*,'(A,1X,F,1X,A)') 'Mean free length:                  ', sys%MeanFreeLength/u_NanoMeter, 'nm.'

  end subroutine

  !----------------------------------------------------------------
  subroutine Propagation(HighestBandExaminedForScatter, band, field, ScatterPointAtBand, this)
    implicit none
    !
    ! Make the incorporation of this parameter(the size for ScatterPointAtBand) cleaner
    integer, intent(in) :: HighestBandExaminedForScatter
    !
    !
    type(band_t),         intent(in)    :: band
    type(field_t),        intent(in)    :: field
    type(ScatterPoint_t), intent(in)    :: ScatterPointAtBand(HighestBandExaminedForScatter)
    type(system_t),       intent(inout) :: this

    ! Internal 
    type(LaserParameter)                                :: LaserSetup
    type(RecombinationMemoryForAllField_t), allocatable :: RecombinationMemoryForAllField(:)
    type(RecombinationMemory_t),            allocatable :: RecombinationMemory(:)
    type(ScatterMemory_t),                  allocatable :: ScatterMemory(:) 
    logical,       parameter :: PrintInformationScatterMemory = .false.
    integer,       parameter :: MaximumLoopAllowed = 100000 ! maximum loop allowed for one excitation time
    integer,       parameter :: ObsInterval = 10
    integer,       parameter :: ProgressIntervalForLaserField = 1
    integer,       parameter :: ProgressIntervalForExcitationTime = 5000
    integer,       parameter :: RecombinationMemoryStepSize = 1000
    logical :: IsMeanFreePathConstraintFulfilled
    logical :: LoopTerminator
    logical :: NewBranchCreated
    logical :: RecombinationOccured
    character(len = 128) :: title_write
    integer :: AccumulatedLoopCountForOneField
    integer :: band_excitation ! the band where the electron lies in when excitation occurs
    integer :: band_initial
    integer :: band_present
    integer :: band_middle
    integer :: band_previous
    integer :: CountRecombinationFromBranchPoint
    integer :: CountRecombinationForOneLaserField
    integer :: i, i_field, i_trj, t
    integer :: LoopCount
    integer :: n_SSsFormula
    integer :: nt
    integer :: PresentBranch
    integer :: RecombinationMemorySize
    integer, allocatable :: PositionRecordSpecification(:)
    real(kind=dp) :: A0
    real(kind=dp) :: f0
    real(kind=dp) :: f0_low
    real(kind=dp) :: f0_up
    real(kind=dp) :: AbsoluteRelativeDistance
    real(kind=dp) :: k_excitation ! the cyrstal momentum where the electron locates at when excitation occurs
    real(kind=dp) :: k_initial
    real(kind=dp) :: k_present
    real(kind=dp) :: k_middle
    real(kind=dp) :: k_previous
    real(kind=dp) :: x_excitation ! the relative distance where the electron-hole pair locates at when excitation occurs
    real(kind=dp) :: x_initial
    real(kind=dp) :: x_present
    real(kind=dp) :: x_previous
    real(kind=dp), allocatable :: At(:)
    real(kind=dp), allocatable :: At_middle(:)
    real(kind=dp), allocatable :: time(:)
    real(kind=dp), allocatable :: x(:)
    real(kind=dp), allocatable :: EnergyOfOneTrajectory(:)
    real(kind=dp), allocatable :: t0(:)
    real(kind=dp), allocatable :: E0(:)
    real(kind=dp), allocatable :: w(:)

    ! WU
    integer :: WU_x, WU_CutOffCoefficientForLinearBand, WU_CutOffEnergy, WU_CutOffEnergyUnitless, WU_field, WU_CutFormula, WU_Return, WU_recombination, WU_EnergyEvolution

    open(newunit = WU_x, file = 'x.dat')
    open(newunit = WU_field, file = 'field.dat')
    open(newunit = WU_CutFormula, file = 'cut_formula.dat')
    open(newunit = WU_Return, file = 'return.dat')

    ! Write down the cut-off energy using Shunsuke's formula
    f0_low = 0.d0*u_VoltOverAngstrom
    f0_up  = 0.3d0*u_VoltOverAngstrom
    n_SSsFormula = 50
    write(WU_CutFormula, *) '# field amplitude(V/A), cut-off energy predicted by SSs" formula.'
    do i = 1, n_SSsFormula
      f0 = dble(i)*(f0_up - f0_low)/dble(n_SSsFormula) + f0_low
      write(WU_CutFormula, *) f0/u_VoltOverAngstrom, SSsFormulaForCutOff(2.d0*pi/field%lambda_min, band%Meff, f0, band%gap)/u_eV
    end do

    ! Set up designation array for position writing
    !   -2th element: for designating a field
    !   -1th element: for designating an excitation time
    !    0th element: state for 0th branch point, and is always 0.
    !    1st~ 10th element: for designating scattter memory
    ! FIXME: add input checker
    allocate(PositionRecordSpecification(-2:this%MaximumNumberOfBranchForScatter))
    PositionRecordSpecification(0:) = 0  
    call Parse('RecordSpecification', 13, PositionRecordSpecification(-2:10))
    PositionRecordSpecification(0) = 0  

    ! Set up excitation assumptions
    band_excitation = 2
    x_excitation = 0.d0
    k_excitation = this%k0

    allocate(ScatterMemory(0:this%MaximumNumberOfBranchForScatter))
    ScatterMemory(:)%state = 0 

    allocate(RecombinationMemoryForAllField(this%n_field))
    nt = int( this%TotalPropagationTimeForOneExcitedTime/this%dt) + 1
    allocate(x(nt))
    allocate(EnergyOfOneTrajectory(nt))
    allocate(time(nt))
    allocate(At(nt))
    allocate(At_middle(nt))

    ! Set up various excitation time
    allocate(t0(this%n_trj))
    if (this%n_trj == 1) then
      t0 = this%T_Prop_0
    else
      do i_trj = 1, this%n_trj
        t0(i_trj) = this%T_Prop_0 + dble(i_trj - 1)*(this%T_Prop_max - this%T_Prop_0)/dble(this%n_trj - 1)
      end do
    end if

    ! Set up various field amplitudes or angular frequencies
    allocate(E0(field%n_field))
    allocate(w(field%n_field))
    ! take the minimum value by default.
    E0 = field%E0_min  
    w = 2.d0*pi/field%lambda_min*u_LightSpeed
    if(field%n_field .ne. 1) then
      do i_field = 1, field%n_field
        if(field%FieldChangingParameter == 1) then
          E0(i_field) = (field%E0_max - field%E0_min)/dble(field%n_field - 1)*dble(i_field - 1) + field%E0_min
        elseif(field%FieldChangingParameter == 2) then
          w(i_field) = (field%lambda_max - field%lambda_min)/dble(field%n_field - 1)*dble(i_field - 1) + field%lambda_min ! equal spaced wavelength
          w(i_field) = 2.d0*pi/w(i_field)*u_LightSpeed
!          w(i_field) =  u_LightSpeed*(2.d0*pi/field%lambda_max - 2.d0*pi/field%lambda_min)/dble(field%n_field - 1)*dble(i_field - 1) + 2.d0*pi/field%lambda_min*u_LightSpeed
        else
          call MessageText('Error','S:Propagation:: Invalid FieldChangingParameter.')
        end if
      end do
    end if

    ! Propagate trajectory for various laser fields.
    do i_field = 1, this%n_field

      AccumulatedLoopCountForOneField = 0

      ! Progress indicator for laser field loop
      if(mod(i_field, ProgressIntervalForLaserField) == 0 .or. i_field == 1) &
        &write(*, '(X,A,X,I,A,X,I,X,A)') 'Progress:', i_field, 'th laser field among all', this%n_field, 'laser fields.'

      ! Set up recombination memory; by default we only record the case of first laser field.
      RecombinationMemorySize = RecombinationMemoryStepSize
      allocate(RecombinationMemory(RecombinationMemorySize))
      CountRecombinationForOneLaserField = 0

      ! Set up the laser
      select case (this%FieldChoice)
      case (1)
        ! This is actually for vector potential
        LaserSetup%envelope = 'constant'
        LaserSetup%amplitude = E0(i_field)/w(i_field)
        LaserSetup%AngularFrequency = w(i_field)
        LaserSetup%PhaseShift = 0.d0
        LaserSetup%TimeShift = 0.d0
        LaserSetup%EnvelopeHeight = 1.d0
      case (2)
        LaserSetup%envelope = 'sine-quartic'
        LaserSetup%amplitude = E0(i_field)/w(i_field)
        LaserSetup%AngularFrequency = w(i_field)
        LaserSetup%PhaseShift = 0.d0
        LaserSetup%TimeShift = 0.d0
        LaserSetup%EnvelopeHeight = 1.d0
        LaserSetup%EnvelopeTime1 = 0.d0
        LaserSetup%EnvelopeProperty1 = 9.d0*2.d0*pi/w(i_field) !96.0664594176350874d0 fs for 3200 nm
        LaserSetup%EnvelopeTime2 = 0.d0
      case default
        call MessageText('Error','S:Propagation:: Invalid FieldChoice.')
      end select

      ! write the vector potential
      write(title_write, *) '# Vector potential for field', i_field
      write(WU_field, *) trim(title_write)
      do t = 1, nt
        call LaserField(LaserSetup, this%dt*dble(t), A0)
        write(WU_field, *) this%dt*dble(t)/u_FemtoSecond, A0
      end do
      write(WU_field, *) ''
      write(WU_field, *) ''

      ! Propagate trajectory of various excitation times
      do i_trj = 1, this%n_trj

        ! Progress indicator for excitation time loop
        if((mod(i_trj, ProgressIntervalForExcitationTime) == 0 .or. i_trj == 1) .and. &
          &this%n_trj > ProgressIntervalForExcitationTime)&
          & write(*, '(3X,A,X,I,A,X,I,X,A)') 'Progress:', i_trj, 'th excitation time among all', this%n_trj, 'times.'

        ! Setup up time, vector potential, and vector potential at middle of time for all 
        !   trajectories starting from this excitation time.
        time = this%dt*dble((/ (t, t = 0, nt - 1) /)) + t0(i_trj)
        call LaserField(LaserSetup, nt, time, At)
        call LaserField(LaserSetup, nt, time + 0.5d0*this%dt, At_middle)

        ! Initialization before a time propagation
        PresentBranch = 0
        ScatterMemory(PresentBranch)%state = 0
        ScatterMemory(PresentBranch)%x = x_excitation
        ScatterMemory(PresentBranch)%k = k_excitation
        if(this%IsMeanFreePathConstraintApplied) ScatterMemory(PresentBranch)%AbsoluteRelativeDistance = 0.d0
        ScatterMemory(PresentBranch)%TimeGrid = 1
        ScatterMemory(PresentBranch)%AccumulatedNumberOfScattering = 0
        ScatterMemory(PresentBranch)%AccumulatedNumberOfRecombination = 0
        ScatterMemory(PresentBranch)%band(0) = band_excitation
        ScatterMemory(PresentBranch)%band(1) = band_excitation
        LoopTerminator = .false.
        LoopCount = 0

        do while(.not.LoopTerminator)

          ! Print the information for the current branch 
          if(PrintInformationScatterMemory) write(*, '(A,X,I,X,A,X,I,X,A,X,10I2)') &
                                           &'Loop count:', LoopCount, 'Present branch:', PresentBranch, &
                                           &'First 10 scatter memory states:', ScatterMemory(1:10)%state

          LoopCount = LoopCount + 1

          ! Initialization for band index, k, and x for the loop.
          ! The variable x_previous may seem redundant but is necessary for avoiding wrong 
          !   recombination detection if x happen to be exactly 0.d0. See more details in
          !   the subroutine DetectorForRecombination  
          band_initial = ScatterMemory(PresentBranch)%band(ScatterMemory(PresentBranch)%state)
          band_present = band_initial 
          band_previous = band_initial
          k_initial = ScatterMemory(PresentBranch)%k
          k_previous = k_initial
          k_present = k_initial
          x_initial = ScatterMemory(PresentBranch)%x
          x_previous = x_initial 
          x_present = x_initial
          CountRecombinationFromBranchPoint = 0
          if(this%IsMeanFreePathConstraintApplied) then
            AbsoluteRelativeDistance = ScatterMemory(PresentBranch)%AbsoluteRelativeDistance
            IsMeanFreePathConstraintFulfilled = .false.
          end if

          ! Data writing at initial time
          if((PositionRecordSpecification(-2) == i_field) .and. &
            &(PositionRecordSpecification(-1) == i_trj)  .and. &
            &sum(abs(PositionRecordSpecification(0:PresentBranch) - ScatterMemory(0:PresentBranch)%state)) == 0) then
            x(ScatterMemory(PresentBranch)%TimeGrid) = x_initial
            EnergyOfOneTrajectory(ScatterMemory(PresentBranch)%TimeGrid) = BandEnergy(band, band_present, k_present) - BandEnergy(band, 1, k_present)
          end if

          !propagation of a trajectory
          do t = ScatterMemory(PresentBranch)%TimeGrid, nt - 1 

            ! k in the middle of the time steps t and t + 1
            k_middle = k_excitation + At_middle(t) - At(1)

            ! k at the time step t + 1
            k_present = k_excitation + At(t + 1) - At(1)

            ! Determine band index for middle time and t+1; also scattering detection.
            call BandIndexAndScatteringHandler()

            ! x at time step t + 1, and record the position if a new branch point is created.
            x_present = x_previous + GroupVelocity(band, band_middle, k_middle)*this%dt
            if(NewBranchCreated) ScatterMemory(PresentBranch)%x = x_present

            ! Check whether Mean-Free path constraint is fulfilled, if such constraint is activated.
            ! If scattering occurs right at the same time grid which mean-free path constraint
            !   is fulfilled, a branch will still be created but immediately shut down by the 
            !   exit statement triggered by IsMeanFreePathConstraintFulfilled. And no 
            !   recombination will occur due to the if statement in calling recombination
            !   detector.
            if(this%IsMeanFreePathConstraintApplied) then
              AbsoluteRelativeDistance = AbsoluteRelativeDistance + abs(x_present - x_previous)
              if(abs(AbsoluteRelativeDistance) >= this%MeanFreeLength) IsMeanFreePathConstraintFulfilled = .true.
            end if

            ! Recombination detections(no recombination allowed when mean-free path constraint is fulfilled)
            if(this%IsMeanFreePathConstraintApplied) then
              if(.not.IsMeanFreePathConstraintFulfilled) call DetectionForRecombination()
            else
              call DetectionForRecombination()
            end if

            ! Data recording
            if((PositionRecordSpecification(-2) == i_field) .and. &
              &(PositionRecordSpecification(-1) == i_trj)  .and. &
              &sum(abs(PositionRecordSpecification(0:PresentBranch) - ScatterMemory(0:PresentBranch)%state)) == 0) then
              if(this%IsMeanFreePathConstraintApplied .and. IsMeanFreePathConstraintFulfilled) then
                x(t+1:) = x_present
                EnergyOfOneTrajectory(t+1:) = BandEnergy(band, band_present, k_present) - BandEnergy(band, 1, k_present)
              else
                x(t+1) = x_present
                EnergyOfOneTrajectory(t+1) = BandEnergy(band, band_present, k_present) - BandEnergy(band, 1, k_present)
              end if
            end if

            ! Setup for the next loop
            x_previous = x_present
            k_previous = k_present
            band_previous = band_present

            ! Exit if mean-free path constraint is fulfilled
            if(this%IsMeanFreePathConstraintApplied .and. IsMeanFreePathConstraintFulfilled) exit

          end do ! end do of a time propagation of the brach of the trajectory of the field

          ! Fall back to previous branch point if the time propagation for the 
          !   scattered and non-scattered paths are completed.
          do while(ScatterMemory(PresentBranch)%state == 1) 
            ScatterMemory(PresentBranch)%state = 0 ! this line is not necessary; added only for ease to view ScatterMemory(:)%state
            PresentBranch = PresentBranch - 1
          end do

          ! Switch to the scattered-path branch 
          !   If current branch is an incompleted branch, this switch to the scattered path.
          !   If the "current branch in do while loop for time propagation" is completed, the 
          !     previous change applied to PresentBranch makes sure here we switch to the 
          !     scattered-path at previous branching point. Therefore, the two cases 
          !     mentioned here happen to yield the same code.
          ScatterMemory(PresentBranch)%state = 1

          ! When present branch is zero, all possible branches are completed, and thereby 
          !   we terminate the loop and go to next excitation time.
          if(PresentBranch == 0) LoopTerminator = .true.

          ! For avoiding infinite do while loop.
          if(LoopCount >= MaximumLoopAllowed) then
            call MessageText('Error','S:Propagation:: Large loop count detected. Simulation is terminated!')
          end if

        end do ! end do of a brach of the trajectory of the field
      
        AccumulatedLoopCountForOneField = AccumulatedLoopCountForOneField + LoopCount

      end do ! end do of a trajectory of the field

      ! Transfer the recombination data in the temporary array to the permanent one. 
      RecombinationMemoryForAllField(i_field)%NumberOfRecombination = CountRecombinationForOneLaserField
      allocate(RecombinationMemoryForAllField(i_field)%RM(CountRecombinationForOneLaserField))
      RecombinationMemoryForAllField(i_field)%RM = RecombinationMemory(1:CountRecombinationForOneLaserField)

      ! Deallocate for usage in the next field.
      deallocate(RecombinationMemory)

      ! Present information about number of loop for this field.
      if(this%ScatterIsAllowed) then
        write(*, '(5X,A,I6,A,I6,A,I6)') '# trajectory / # excitation times = ', &
                                     &AccumulatedLoopCountForOneField, &
                                     &' / ', &
                                     &this%n_trj, &
                                     &' ~ ', NINT(real(AccumulatedLoopCountForOneField)/real(this%n_trj))
        write(*, *) ' '
      end if

    end do ! end do of a field

    call RearrangeRecombinationMemoryForAllField()
    call WriteDataRecombinationEvent()
    call WriteDataPostion()
    call WriteDataEnergyEvolution()
    call WriteDataCutOffEnergy()


    contains 


      subroutine DetectionForScatterPointCrossing(IsCrossed, ScatterPointCrossed, band_prev, k_prev, k_pres)
        implicit none
        logical, intent(out) :: IsCrossed
        integer, intent(out) :: ScatterPointCrossed
        integer, intent(in) :: band_prev ! band at t
        real(kind=dp), intent(in) :: k_prev ! k at t
        real(kind=dp), intent(in) :: k_pres ! k at t+1 or middle time

        IsCrossed = any((ScatterPointAtBand(band_prev)%AtPoint(:) - k_prev)*&
                       &(ScatterPointAtBand(band_prev)%AtPoint(:) - k_pres) < 0.d0)

        ScatterPointCrossed = minloc((ScatterPointAtBand(band_prev)%AtPoint(:) - k_prev)*&
                                    &(ScatterPointAtBand(band_prev)%AtPoint(:) - k_pres), 1)

      end subroutine DetectionForScatterPointCrossing


      subroutine BandIndexAndScatteringHandler()
        implicit none
        ! Internal
        logical :: k_middle_IsCrossing
        logical :: k_present_IsCrossing
        integer :: ScatterPointCrossed

        NewBranchCreated = .false.
        
        if(this%ScatterIsAllowed) then

          call DetectionForScatterPointCrossing(k_middle_IsCrossing, ScatterPointCrossed, &
                                               &band_previous, k_previous, k_middle)
          call DetectionForScatterPointCrossing(k_present_IsCrossing, ScatterPointCrossed, &
                                               &band_previous, k_previous, k_present)

          ! Determine the band_present in case of scattering point crossing is detected
          if(k_present_IsCrossing) then
            ! If number of scattering has not yet reached the maximum, scattering occurs.
            ! Note that it is ">", not ">=", because in the case of "=" the scattered path of this
            !   branch point will have n_scatter_max+1 scattering, which is not allowed.

            if(this%n_scatter_max > ScatterMemory(PresentBranch)%AccumulatedNumberOfScattering + &
                                    &ScatterMemory(PresentBranch)%state) then
              NewBranchCreated = .true.
              PresentBranch = PresentBranch + 1
              ! Store all scattering-related information for this crossing/branching point.
              ScatterMemory(PresentBranch)%TimeGrid = t + 1
              ScatterMemory(PresentBranch)%k = k_present
              if(this%IsMeanFreePathConstraintApplied) ScatterMemory(PresentBranch)%AbsoluteRelativeDistance = AbsoluteRelativeDistance
              ScatterMemory(PresentBranch)%state = 0
              ScatterMemory(PresentBranch)%band(0) = band_previous + &
                                                    &ScatterPointAtBand(band_previous)%&
                                                    &AtPointToward(ScatterPointCrossed)
              ScatterMemory(PresentBranch)%band(1) = band_previous 
              ScatterMemory(PresentBranch)%AccumulatedNumberOfScattering = &
                &ScatterMemory(PresentBranch - 1)%AccumulatedNumberOfScattering + &
                &ScatterMemory(PresentBranch - 1)%state
              ScatterMemory(PresentBranch)%AccumulatedNumberOfRecombination = &
                &ScatterMemory(PresentBranch - 1)%AccumulatedNumberOfRecombination + &
                &CountRecombinationFromBranchPoint
              ! Reset the recombination counter for this branching point.
              CountRecombinationFromBranchPoint = 0
              ! Set band_present to the non-scattered path(our default choice)
              band_present = ScatterMemory(PresentBranch)%band(0)
            else
              ! Otherwise the scatter point is crossed but scattering is just neglect. Move the 
              !   electron to the proper band.
              ! Note that the value of band_present is exactly the same as that when scattering
              !   occurs. This is just a coincidence as we choose to propagate the non-scattered
              !   path by default.
              band_present = band_previous + &
                            &ScatterPointAtBand(band_previous)%AtPointToward(ScatterPointCrossed)
            end if
          end if

          ! determine band_middle
          if(k_middle_IsCrossing .and. k_present_IsCrossing) then
            band_middle = band_present
          else
            band_middle = band_previous
            ! Note that I choose to set band_middle as band_previous
            !   in the case where k_middle crosses scatter point but k_present.
            !   The reason is that, since electron at k_present is not scattered 
            !   at all, electron scattered at k_midle is considered as false scattering
            !   if an electron does scatter at k_middle.
          end if

        else

          ! This makes sure for parabolic and Kane's bands an electron go 
          !   to the upper/lower band correctly when it passes a zone boundary
          !   in the case where scattering is not considered. This is necessary 
          !   as we assume, by default, electrons remain in the same band when 
          !   passing the boundary.
          if((band%BandChoice == 1 .or. band%BandChoice == 2 .or. band%BandChoice == 3)) then
            band_middle = 2 + int(abs(k_middle)/(0.5d0*band%ReciprocalLatticeVector))
            band_present = 2 + int(abs(k_present)/(0.5d0*band%ReciprocalLatticeVector))
          end if

        end if

      end subroutine BandIndexAndScatteringHandler


      subroutine DetectionForRecombination()
        implicit none
        type(RecombinationMemory_t), allocatable :: RecombinationMemoryTmp(:)

        ! Detection algorithm
        ! Note that by using x_previous*x_present < 0 as the algorithm for recombination detection,
        !   this algorithm yields no recombination if x =0 occurs(though unlikely in the numerical
        !   sense). If one changes the algorithm to x_previous*x_present <= 0, then one extra 
        !   recombination will occurs. Some examples are: 
        !   x(t-1)   x(t)   x(t+1)
        !   <0       =0     <0    => 0 or 2 recombination, respectively.
        !   <0       =0     >0    => 0 or 2 recombination, respectively.
        !   There would be more cases of wrong detections if one consider more times evolving x=0.
        ! We ASSUME x will not be zero numerically, except for the time of excitaiton where x is 
        !   physically assumed to be exactly zero. I handle this simply by switching off 
        !   recombination detection at excitation time.
        RecombinationOccured = x_present*x_previous < 0.d0
        if(t == 1) RecombinationOccured = .false.

        ! Record the recombination data.
        if(RecombinationOccured) then
          CountRecombinationForOneLaserField = CountRecombinationForOneLaserField + 1
          CountRecombinationFromBranchPoint = CountRecombinationFromBranchPoint + 1
          ! Increase the array size for recombination memory if it's not enough for storing 
          !   the data.
          if(RecombinationMemorySize < CountRecombinationForOneLaserField) then
            allocate(RecombinationMemoryTmp(RecombinationMemorySize))
            RecombinationMemoryTmp = RecombinationMemory
            deallocate(RecombinationMemory)
            RecombinationMemorySize = RecombinationMemorySize + RecombinationMemoryStepSize
            allocate(RecombinationMemory(RecombinationMemorySize))
            RecombinationMemory(1:RecombinationMemorySize - RecombinationMemoryStepSize) = RecombinationMemoryTmp(:)
            deallocate(RecombinationMemoryTmp)
          end if
          ! Recording.
          RecombinationMemory(CountRecombinationForOneLaserField)%NumberOfRecombination = ScatterMemory(PresentBranch)%AccumulatedNumberOfRecombination + CountRecombinationFromBranchPoint 
          RecombinationMemory(CountRecombinationForOneLaserField)%NumberOfScattering = ScatterMemory(PresentBranch)%AccumulatedNumberOfScattering + ScatterMemory(PresentBranch)%state
          RecombinationMemory(CountRecombinationForOneLaserField)%Energy = BandEnergy(band, band_present, k_present) - BandEnergy(band, 1, k_present)
          RecombinationMemory(CountRecombinationForOneLaserField)%TimeExcitation = time(1)
          RecombinationMemory(CountRecombinationForOneLaserField)%TimeRecombination = time(t)
        end if

      end subroutine DetectionForRecombination


      subroutine RearrangeRecombinationMemoryForAllField()
        ! Rearrange the recombination memory array such that it is sorted first with the number
        !   of scattering and then number of recombination.
        implicit none
        ! Internal
        integer :: n  ! number of recombination
        integer :: m  ! number of recombination for a fixed number of scattering
        integer :: lower, upper ! beginning and ending index for a fixed number of scatteirng
        integer :: i_field
        integer :: i, j
        integer, allocatable :: mapping(:)
        integer, allocatable :: NumberOfElementForEachScattering(:)
        type(RecombinationMemory_t), allocatable :: TRM(:) ! Temporary Recombination Memory

        allocate(NumberOfElementForEachScattering(0:this%n_scatter_max))

        do i_field = 1, this%n_field
          n = RecombinationMemoryForAllField(i_field)%NumberOfRecombination

          ! Sorting according to number of scattering
          allocate(TRM(n))
          allocate(mapping(n))
          call MappingIndexForSortingIntegerArray(n, RecombinationMemoryForAllField(i_field)%RM(:)%NumberOfScattering, mapping)
          do i = 1, n
            TRM(i) = RecombinationMemoryForAllField(i_field)%RM(mapping(i))
          end do
          RecombinationMemoryForAllField(i_field)%RM = TRM
          deallocate(TRM)
          deallocate(mapping)

          ! Check the number of elements for a fixed number of scattering
          NumberOfElementForEachScattering = 0
          do i = 1, n
            NumberOfElementForEachScattering(RecombinationMemoryForAllField(i_field)%RM(i)%NumberOfScattering) = &
              &NumberOfElementForEachScattering(RecombinationMemoryForAllField(i_field)%RM(i)%NumberOfScattering) + 1
          end do

          ! Sorting according to number of recombination
          do j = 0, this%n_scatter_max
            m = NumberOfElementForEachScattering(j)
            if(m >= 1) then
              allocate(TRM(m))
              allocate(mapping(m))
              upper = sum(NumberOfElementForEachScattering(0:j))
              lower = upper - m + 1
              call MappingIndexForSortingIntegerArray(m, RecombinationMemoryForAllField(i_field)%RM(lower:upper)%NumberOfRecombination, mapping)
              do i = 1, m
                TRM(i) = RecombinationMemoryForAllField(i_field)%RM(mapping(i) + lower - 1)
              end do
              RecombinationMemoryForAllField(i_field)%RM(lower:upper) = TRM
              deallocate(TRM)
              deallocate(mapping)
            end if
          end do
        end do

      end subroutine


      subroutine WriteDataRecombinationEvent()
        implicit none
        ! Internal
        integer :: i, j, i_field
        integer :: upper, lower
        integer, allocatable :: NumberOfElementForEachScattering(:)

        allocate(NumberOfElementForEachScattering(0:this%n_scatter_max))

        i_field = 1 ! only write down the 1st field though we have data for all field; the extension to multi-field writing is not simple

        ! Check the number of elements for a fixed number of scattering
        NumberOfElementForEachScattering = 0
        do i = 1, RecombinationMemoryForAllField(i_field)%NumberOfRecombination
          NumberOfElementForEachScattering(RecombinationMemoryForAllField(i_field)%RM(i)%NumberOfScattering) = &
            &NumberOfElementForEachScattering(RecombinationMemoryForAllField(i_field)%RM(i)%NumberOfScattering) + 1
        end do

        do i = 0, this%n_scatter_max
          if(i > 9)  call MessageText('Error','S:Propagation::WriteDataRecombinationEvent:: Maximum number of scattering for printing data should be <= 9 for correct file naming.')
          if(NumberOfElementForEachScattering(i) >= 1) then
            upper = sum(NumberOfElementForEachScattering(0:i))
            lower = upper - NumberOfElementForEachScattering(i) + 1

            write(title_write, '(A,I1,A)') 'RecombinationWith', i, 'Scattering.dat'
            open(newunit = WU_Recombination, file = title_write)

            write(WU_Recombination, '(A,I)') '# Recombination Data for Field: ', i_field
            write(WU_Recombination, '(7A)') '# Time of Recombination(OC)', &
                                         &'  Energy of Recombination(eV)', &
                                         &'  Return Time(OC)', &
                                         &'  Gained Energy / Ponderomotive Energy', &
                                         &'  Time of Excitation(OC)', &
                                         &'  Number of Recombination', &
                                         &'  Number of Scattering'
            do j = lower, upper
              write(WU_Recombination, '(5ES,X,2I)') RecombinationMemoryForAllField(i_field)%RM(j)%TimeRecombination/this%period, &
                                                   &RecombinationMemoryForAllField(i_field)%RM(j)%Energy/u_eV, &
                                                   &(RecombinationMemoryForAllField(i_field)%RM(j)%TimeRecombination - RecombinationMemoryForAllField(i_field)%RM(j)%TimeExcitation)/this%period, &
                                                   &(RecombinationMemoryForAllField(i_field)%RM(j)%Energy - band%gap)/this%PonderomotiveEnergyMin, &
                                                   &RecombinationMemoryForAllField(i_field)%RM(j)%TimeExcitation/this%period, &
                                                   &RecombinationMemoryForAllField(i_field)%RM(j)%NumberOfRecombination, &
                                                   &RecombinationMemoryForAllField(i_field)%RM(j)%NumberOfScattering
            end do
            close(WU_Recombination)
          end if
        end do

      end subroutine

      
      subroutine WriteDataCutOffEnergy()
        implicit none
        ! Internal
        integer :: i, j, i_field
        integer :: upper, lower
        integer :: LocationForCutOff
        integer, allocatable :: NumberOfElementForEachScattering(:)
        real(kind=dp) :: PonderomotiveEnergy

        allocate(NumberOfElementForEachScattering(0:this%n_scatter_max))

        do i = 0, this%n_scatter_max

          if(i > 9)  call MessageText('Error','S:Propagation::WriteDataCutOffEnergy:: Maximum number of scattering for printing data should be <= 9 for correct file naming.')
          write(title_write, '(A,I1,A)') 'CutOffEnergyWith', i, 'Scattering.dat'
          open(newunit = WU_CutOffEnergy, file = title_write)
          write(title_write, '(A,I1,A)') 'CutOffEnergyWith', i, 'ScatteringUnitless.dat'
          open(newunit = WU_CutOffEnergyUnitless, file = title_write)
          write(title_write, '(A,I1,A)') 'CutOffCoefficientForLinearBandWith', i, 'Scattering.dat'
          open(newunit = WU_CutOffCoefficientForLinearBand, file = title_write)
          if(field%FieldChangingParameter == 1) then
            write(WU_CutOffEnergy, '(4A)')                   '# Field amplitude(V/A)', ' Cut-off energy(eV)', ' Number of recombination', ' Number of scattering'
            write(WU_CutOffEnergyUnitless, '(4A)')           '# sqrt(Ponderomotive Energy / Band Gap)', ' Cut-off energy / Band Gap', ' Number of recombination', ' Number of scattering'
            write(WU_CutOffCoefficientForLinearBand, '(4A)') '# 2*sqrt[Ponderomotive energy*gap', ' Cut-off energy/(2*sqrt[Ponderomotive energy*gap])', ' Number of recombination', ' Number of scattering'
          elseif(field%FieldChangingParameter == 2) then
            write(WU_CutOffEnergy, '(4A)') '# Wavelength(nm)', ' Cut-off energy(eV)', ' Number of recombination', ' Number of scattering'
          end if

!
! FIXME: the way to determine size of fixed scattering and writing data are chaotic. fix in the future
!
          do i_field = 1, this%n_field
            ! Check the number of elements for a fixed number of scattering
            NumberOfElementForEachScattering = 0
            do j = 1, RecombinationMemoryForAllField(i_field)%NumberOfRecombination
              NumberOfElementForEachScattering(RecombinationMemoryForAllField(i_field)%RM(j)%NumberOfScattering) = &
                &NumberOfElementForEachScattering(RecombinationMemoryForAllField(i_field)%RM(j)%NumberOfScattering) + 1
            end do
            upper = sum(NumberOfElementForEachScattering(0:i))
            lower = upper - NumberOfElementForEachScattering(i) + 1

            if(NumberOfElementForEachScattering(i) >= 1) then
              LocationForCutOff = maxloc(RecombinationMemoryForAllField(i_field)%RM(lower:upper)%Energy, 1)
              if(field%FieldChangingParameter == 1) then
                PonderomotiveEnergy = (E0(i_field)/2.d0/w(i_field))**2.d0/band%Meff
                write(WU_CutOffEnergy, '(2ES,2I)') E0(i_field)/u_VoltOverAngstrom, &
                                                  &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%Energy/u_eV, &
                                                  &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfRecombination, &
                                                  &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfScattering
                write(WU_CutOffEnergyUnitless, '(2ES,2I)') sqrt(PonderomotiveEnergy/band%gap), &
                                                          &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%Energy/band%gap, &
                                                          &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfRecombination, &
                                                          &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfScattering
                write(WU_CutOffCoefficientForLinearBand, '(2ES,2I)') 2.d0*sqrt(PonderomotiveEnergy*band%gap), & 
                                                                &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%Energy/(2.d0*sqrt(PonderomotiveEnergy*band%gap)), &
                                                                &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfRecombination, &
                                                                &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfScattering
              elseif(field%FieldChangingParameter == 2) then
                write(WU_CutOffEnergy, '(2ES,2I)') 2.d0*pi/w(i_field)*u_LightSpeed/u_NanoMeter, &
                                                  &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%Energy/u_eV, &
                                                  &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfRecombination, &
                                                  &RecombinationMemoryForAllField(i_field)%RM(lower - 1 + LocationForCutOff)%NumberOfScattering
              end if
            end if
          end do
        end do

      end subroutine


      subroutine WriteDataPostion()
        implicit none

        write(WU_x, *) '# x(t) for trajectory: ', PositionRecordSpecification(-1), &
                      &'under field:', PositionRecordSpecification(-2)
        write(WU_x, '(X,A,X,10I)') '# first 10 scatter specification:', PositionRecordSpecification(1:10)
        write(title_write, *) '# Time(OC), Postion(Lattice constant)'

        call WriteDataCurve2D_DoubleReal(WU_x, trim(title_write), nt/ObsInterval, x, time, &
                                        &this%LatticeConstant, this%period, 0, nt)

      end subroutine


      subroutine WriteDataEnergyEvolution()
        implicit none

        open(newunit = WU_EnergyEvolution, file = 'EnergyEvolution.dat')
        write(WU_EnergyEvolution, *) '# Energy evolution for trajectory: ', PositionRecordSpecification(-1), &
                                    &'under field:', PositionRecordSpecification(-2)
        write(WU_x, '(X,A,X,10I)') '# first 10 scatter specification:', PositionRecordSpecification(1:10)
        write(title_write, *) '# Time(OC), Energy(eV) '
        call WriteDataCurve2D_DoubleReal(WU_EnergyEvolution, trim(title_write), nt/ObsInterval, &
                                        &EnergyOfOneTrajectory, time, u_eV, this%period, 0, nt)

      end subroutine

  end subroutine Propagation


  ! Ascending sorting
  subroutine MappingIndexForSortingIntegerArray(n, array, mapping)
    implicit none
    integer,intent(in) :: n
    integer, intent(in) :: array(n)
    integer, intent(out) :: mapping(n)
    ! Internal
    logical :: mask(n)
    integer :: i
    integer :: LocationForMinimum

    mask = .true.
    do i = 1, n
      LocationForMinimum = minloc(array, 1, mask)
      mapping(i) = LocationForMinimum
      mask(LocationForMinimum) = .false.
    end do

  end subroutine


end module GlobalMDL
